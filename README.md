# descartes

Lists of methods for the [dato](https://squeak.eauchat.org/dato/) [zeus](https://squeak.eauchat.org/zeus/) library.

For the full documentation, installation instructions... check [descartes documentation page](https://squeak.eauchat.org/libs/descartes/).
