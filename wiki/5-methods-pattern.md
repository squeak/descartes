# The pattern methods must follow
🛈 This page uses squyntax notation, information/documentation about it is available [here](https://framagit.org/squeak/squyntax). 🛈

The following type describes the shape that methods listed in descartes must have:
```javascript
methodObject = <{
  ?description: <string> «
    description message,
    the label of the method returned by descartes.list will contain:
      methodName,
      methodObject.description (if defined)
      something about mandatory number of arguments (if defined)
  »,
  ?arguments: <inputify·options[]|null> «
    description of the arguments that should be passed
    if not defined, user will be free to input as many arguments as desired
    if null or empty array, the user will not be allowed to set any argument
    never use the "name"/"key" option when defining those arguments (if you want to display a nice name for an argument, use the "label" option)
  »,
  ?argumentsModel: <inputify·options> «
    you can use this to specify the type of input that all arguments should have
    for example if the function can receive any number of arguments all of the same type
  »,
  !method: <function(
    zeusArgs <any[]> « the list of arguments defined when declaring the use of this method (= "arg", "um", and "ents" in 'zeus+meth.od.name("arg", "um", "ents")') »,
    ... any other arguments that are passed when the function is executed in the corresponding library
  )> «
    the method to execute
    to access to the arguments passed in the upper "arguments" option, use zeusArgs[ the number index of your argument ]
  »,
}>
```
