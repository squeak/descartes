# Usage

Load and use descartes library with:

```javascript
const descartes = require("descartes");
decartes.autofillMethods(objectContainingSomeKeysOrSubkeysWithMethodStrings);
```

## Or in more details

This:
```javascript
decartes.autofillMethods({
  some: "string",
  somethingElse: true,
  aKeyWhichValueWillBeReplacedByDescartes: 'zeus+display.color.result("#FF00FF")',
  aKeyWithFunc_whyNot: function () { return "hello"; },
  myCustomizedIcon: "rocket",
  anObject: {
    with: "nested",
    methodToReplace: 'zeus+display.icon.valueOrResult("myCustomizedIcon", "question2")',
  },
});
```

Will return this:
```javascript
{
  some: "string",
  somethingElse: true,
  aKeyWhichValueWillBeReplacedByDescartes: _.partial(function (zeusArgs, entry) {
    return zeusArgs[0];
  }, [ "#FF00FF" ]),
  aKeyWithFunc_whyNot: function () { return "hello"; },
  myCustomizedIcon: "rocket",
  anObject: {
    with: "nested",
    methodToReplace: _.partial(function (zeusArgs, entry) {
      var iconInEntry = $$.getValue(entry, zeusArgs[0]);
      var fallbackIcon = zeusArgs[1]
      return iconInEntry || fallbackIcon;
    }, [ "myCustomizedIcon", "question2" ]),
  },
};
```
