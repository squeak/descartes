
Descartes is a library of built-in methods that can be used in [dato apps](https://framagit.org/squeak/dato).

If you are not programming something for [dato](/dato/) or an app based on [zeus](/zeus/), there is not much that descartes will do for you. He's not so open minded, he keeps track of his methods, some of which will never even work outside of this environment.
In other words, he's like our old friend descartes, likes to organize, doesn't see much that he's enclosed in a little bubble, imagine glories and universalities. But in fact he's in his little bubble.
