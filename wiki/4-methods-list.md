# All available methods

It would be nice to have documented an inventory of all the available methods (and families).
Unfortunately, it hasn't been done. Feel welcome to contribute, or just visit [descartes repository](https://framagit.org/squeak/descartes/) and browse the <methods> directory to get an overview and detailed explanation of everything available.
