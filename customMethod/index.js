var methodCategories = require("./categories");
var descartesConfig = require("../config");

// UTILITIES SO THAT THEY ARE USABLE IN CUSTOM METHODS
var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension");
var $ = require("yquerj");
var uify = require("uify");
require("uify/extension/swiper");
var async = require("async");
var descartesUtils = require("../utilities");
var markdownify = require("markdownify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LIST AVAILABLE METHODS CHOICES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: list available methods (created by the user in it's types database)
  ARGUMENTS: (
    !methodCategory <string> « should be the name of a method category listed in ./methodCategories.js »,
    !callback <function(<inputify·options.choices>),
  )
  RETURN: <void>
*/
function availableMethodsChoices (methodCategory, callback) {

  descartesUtils.pouch.entries.fetchAll("dato", {
    type: "_method_",
    category: methodCategory,
  }, function (entriesList) {
    callback(_.pluck(entriesList, "name"));
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (methodCategory) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var methodCategorySettings = _.findWhere(methodCategories, { categoryName: methodCategory, });
  var argumentList = _.pluck(methodCategorySettings.arguments, "name").join(", ");

  return {
    description: "Use one of your own customized function (you can create new methods from the list of methods page, that you can access from the navigation panel).",
    arguments: [
      {
        label: "Method to use: ",
        labelLayout: "stacked",
        type: "select",
        choices: function (callback) { availableMethodsChoices(methodCategory, callback); },
      },
      {
        label: "Additional arguments: ",
        type: "object",
        labelLayout: "stacked",
        object: { model: { type: "any", }, },
      },
    ],
    method: function (zeusArgs) {

      var methodObject = _.findWhere(descartesConfig.additionalMethods, {
        name: zeusArgs[0],
        category: methodCategory,
      });
      if (!methodObject) return $$.log.error("[descartes] Could not find custom method: "+ methodCategory +"."+ zeusArgs[0]);

      var methodFunc;
      try { eval("methodFunc = function (additionalArguments, "+ argumentList +") { "+ methodObject.code +" }"); }
      catch (e) { $$.log.error("Could not evaluate custom method: "+ methodObject.name); };

      if (!_.isFunction(methodFunc)) $$.log.error("[descartes] You didn't specify the method to use.")
      else {
        var argsToPass = $$.array.merge(zeusArgs[1] || {}, _.rest(arguments));
        return methodFunc.apply(this, argsToPass);
      };

    },
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
