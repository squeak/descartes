var _ = require("underscore");

module.exports = _.flatten([
  require("./action"),
  require("./db"),
  require("./display"),
  require("./edify"),
  require("./event"),
  require("./graphify"),
  require("./inputify"),
  require("./tablify"),
  require("./viewify"),
], true);
