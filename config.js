
module.exports = {

  // current user
  username: "visitor",

  // list of additional methods this user has
  additionalMethods: [],

  // cache to keep track of which pouch databases have been created (in zeus, tablify, descartes)
  pouchsCache: {},

};
