var _ = require("underscore");
var $$ = require("squeak");
var methods = require("./methods");

var descartes = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              CONFIGURATION

  config: require("./config"),

  //
  //                              GET

  /**
    DESCRIPTION: get a descartes method
    ARGUMENTS: ( !methodDeepKey <string> « deep key where to access this method » )
    RETURN: <function|undefined>
  */
  getBuiltinMethod: function (methodDeepKey) {
    var method = $$.getValue(methods, methodDeepKey);
    if (!$$.isObjectLiteral(method)) return $$.alert.detailedError(
      "descartes.getBuiltinMethod",
      "The method you try to use doesn't exist: "+ methodDeepKey
    )
    else return method.method;
  },

  //
  //                              LIST

  /**
    DESCRIPTION: list the available descartes methods in the given family
    ARGUMENTS: ( !family <string> « the key/name of the family you want to get the list of methods » )
    RETURN: <methodChoiceObject[]|[]> « the list of methods names available in this family of methods »
    TYPES:
      methodChoiceObject = <{
        label: <string>,
        value: <string>,
      }>
  */
  list: function (family) {
    return _.map($$.getValue(methods, family), function (methodFuncOrObject, methodName) {
      var label = methodName;
      if (_.isObject(methodFuncOrObject)) {
        label = '<span>'+ label;
        if (methodFuncOrObject.description) label += '<br><small>'+ methodFuncOrObject.description +'</small>';
        if (_.isArray(methodFuncOrObject.arguments) || _.isNull(methodFuncOrObject.arguments)) {
          if (_.isNull(methodFuncOrObject.arguments) || methodFuncOrObject.arguments.length === 0) label += "<br><small>no arguments</small>"
          else label += "<br><small>"+ methodFuncOrObject.arguments.length +" argument(s)</small>";
        };
        label += '</span>';
      };
      return {
        _isChoice: true,
        arguments: methodFuncOrObject ? methodFuncOrObject.arguments : undefined,
        argumentsModel: methodFuncOrObject ? methodFuncOrObject.argumentsModel : undefined,
        label: label,
        value: methodName,
      };
    });
  },

  //
  //                              ZEUS NAMING LIST

  /**
    DESCRIPTION: same as list, but will prepend "zeus+<familyName>" before each method's name (e.g. ["zeus+callback.confirm", ...])
    ARGUMENTS: ( see descartes.list )
    RETURN: see descartes.list
  */
  zeusList: function (familyName) {
    return _.map(descartes.list(familyName), function (methodChoiceObject) {
      methodChoiceObject.value = "zeus+"+ familyName +"."+ methodChoiceObject.value;
      return methodChoiceObject;
    });
  },

  //
  //                              GET METHOD NAME AND ARGUMENTS

  /**
    DESCRIPTION: explode a zeus method string (e.g. "zeus+my.method(some, arg, uments)" or "zeus+otherMethod.without.args")
    ARGUMENTS: ( !string <"zeus+<methodName>(<methodArgs>)"> « the string containing method name and arguments » )
    RETURN: <{
      prefix: <"zeus+">,
      deepKey: <string|number>,
      arguments: <any[]>
    }>
  */
  getMethodNameAndArguments: function (string) {

    var methodDeepKey = string.replace(/^zeus\+/, "");
    var args = $$.match(methodDeepKey, /\(.*\)*$/);
    if (args) {
      methodDeepKey = methodDeepKey.replace(/\(.*\)*$/, "");
      args = args.replace(/^\(/, "[").replace(/\)$/, "]");
      args = $$.json.parseIfJson(args, function () {
        $$.log.detailedError(
          "descartes.getMethodNameAndArguments",
          "error trying to parse descartes arguments",
          args
        );
      });
    };

    return {
      prefix: "zeus+",
      deepKey: methodDeepKey,
      arguments: args,
    };

  },

  //
  //                              AUTOFILL METHODS

  /**
    DESCRIPTION: replace any value that has starts with "zeus+" with the corresponding method
    ARGUMENTS: ( configOrSubParameter <any> )
    RETURN: <config> « returned but it's the same as the one give, it has been modified »
  */
  autofillMethods: function (configOrSubParameter) {

    _.each(configOrSubParameter, function (value, key) {

      // replace any value that starts with "zeus+" with the corresponding method
      if (_.isString(value) && value.match(/^zeus\+/)) {
        var method = descartes.getMethodNameAndArguments(value);
        var methodFunc = descartes.getBuiltinMethod(method.deepKey);
        if (!methodFunc) console.error("Could not find zeus method: "+ method.deepKey +"\n(Full method string was: "+ value +")");
        configOrSubParameter[key] = methodFunc ? _.partial.call(this, methodFunc, method.arguments || []) : value;
      }
      // recurse if object
      else if (_.isObject(value)) descartes.autofillMethods(value);

    });

    return configOrSubParameter;

  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = descartes;
