
module.exports = {

  inputify: require("inputify"),

  guitarChords: require("./guitarChords"),
  pouch: require("./pouch"),
  wikipediaFetcher: require("./wikipediaFetcher"),
  conditions: require("./conditions"),
  duckduckgoSearcher: require("./duckduckgoSearcher"),
  getDatabaseEntries: require("./getDatabaseEntries"),
  graphutils: require("./graphutils"),
  multinterpolate: require("./multinterpolate"),
  notifications: require("./notifications"),
  subjectAndMonitoring: require("./subjectAndMonitoring"),

};
