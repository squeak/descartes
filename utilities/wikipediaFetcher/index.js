var _ = require("underscore");
var $$ = require("squeak");
var presets = require("./presets");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: fetch some data from a wikipedia article
  ARGUMENTS: (
    !urlOrArticleName <string> «
      url of the wikipedia article you want to fetch data of
      or simply the article name
    »,
    !fields <{
      !localKey: <string> « the key of this entry locally »,
      !dbpediaKey: <string> « the key of this entry for wikipedia »,
      ?process: <function(value):<any>> « the return of this function will define the value for this key if you don't want it raw as it's set in wikipedia »,
    }[]> « the list of keys to fetch data for in the article »,
    ?lang <string>@default="en" « if content is in multiple languages, the language you want to get it in »,
  })
  RETURN: <Promise.then(result)>
*/
var wikipediaFetcher = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  main: require("./wikifetcher"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PRESET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: fetch some data from a wikipedia article, fetching the preset fields and post processing them as preset
    ARGUMENTS: (
      !wikipediaUrl <string> « url of the wikipedia article you want to fetch data of (must be a english wikipedia page for now) »,
      !presetName <"film">,
    )
    RETURN: <Promise.then(result)>
  */
  preset: function (wikipediaUrl, presetName) {

    var presetConfig = presets[presetName];

    // UNSUPPORTED
    if (!presetConfig) return Promise.reject(new Error("This type of article is not (yet) supported by wikipediaFetcher."))

    // FETCH
    else return wikipediaFetcher(wikipediaUrl, presetConfig.fields, presetConfig.lang).then(function (result) {

      // optional post processing
      if (presetConfig.postProcess) result = presetConfig.postProcess(result);

      return result;

    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

module.exports = wikipediaFetcher;
