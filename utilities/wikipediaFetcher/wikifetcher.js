var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DBPEDIA URI TO PREFIXES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var prefixes = {
  rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
  rdfs: "http://www.w3.org/2000/01/rdf-schema#",
  xsd: "http://www.w3.org/2001/XMLSchema#",
  owl: "http://www.w3.org/2002/07/owl#",
  dc: "http://purl.org/dc/terms/",
  foaf: "http://xmlns.com/foaf/0.1/",
  vcard: "http://www.w3.org/2006/vcard/ns#",
  dbp: "http://dbpedia.org/property/",
  dbo: "http://dbpedia.org/ontology/",
  geo: "http://www.geonames.org/ontology#",
  wgs: "http://www.w3.org/2003/01/geo/wgs84_pos#",
  dbr: "http://dbpedia.org/resource/",
  georss: "http://www.georss.org/georss/",
  prov: "http://www.w3.org/ns/prov#",
  gold: "http://purl.org/linguistics/gold/",
  schema: "http://schema.org/",
};

/**
  DESCRIPTION: convert the given prefix URI to a simple dbpedia prefix
  ARGUMENTS: (
    !prefixURI <string> « uri to get prefix of »,
    ?stripURI <boolean> « if true, will not replace uri by prefix, but simply strip uri »
    ?silent <boolean> « if true, will not show an alert if prefix could not be converted »
  )
  RETURN: <string|undefined>
*/
function convertUriToPrefix (prefixURI, stripURI, silent) {
  var found = false;
  for (var prefixCode in prefixes) {
    if (prefixURI.match(prefixes[prefixCode])) {
      found = true;
      if (stripURI) return prefixURI.replace(prefixes[prefixCode], "")
      else return prefixCode +":"+ prefixURI.replace(prefixes[prefixCode], "");
    };
  };
  if (!silent && found === false) $$.log.error("Failed to find a matching prefix for key: "+ prefixURI);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PROCESS DBPEDIA RESPONSE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: extract the property in the desired language in the passed list of value objects
  ARGUMENTS: (
    !dbpediaPropObject <object[]> « array of value objects, each in a language »,
    ?desiredLanguage <string>@default="en" « language prefix of the language you want the value in »,
  )
  RETURN: <object>
*/
function extractPropertyValueFromLanguagesArray (dbpediaPropObject, desiredLanguage) {

  var objContainingValue;

  // get value in desired language
  if (desiredLanguage !== "en") objContainingValue = _.findWhere(dbpediaPropObject, { lang: desiredLanguage, });
  // get value in english
  if (_.isUndefined(objContainingValue)) objContainingValue = _.findWhere(dbpediaPropObject, { lang: "en", });
  // still nothing, get first object in array
  if (_.isUndefined(objContainingValue)) objContainingValue = dbpediaPropObject[0];
  // still nothing, log it
  if (_.isUndefined(objContainingValue)) $$.log.warning("Could not extract value object from the following dbpedia property object:", dbpediaPropObject);

  return objContainingValue;

};

/**
  DESCRIPTION: convert dbpedia response to an object that's more understandable
  ARGUMENTS: (
    dbpediaResponse <object>,
    desiredLanguage <string>,
  )
  RETURN: <object>
*/
function interpretDBpediaResponse (dbpediaResponse, desiredLanguage) {

  // CONVERT PROPERTY URLS INTO PROPERTY NAMES
  var withPropertyNames = {};
  _.each(dbpediaResponse, function (dbpediaValue, dbpediaKey) {
    var prefix = convertUriToPrefix(dbpediaKey);
    if (prefix) withPropertyNames[prefix] = dbpediaValue;
  });

  // CONVERT VALUES OBJECT INTO PROPER VALUES
  var withProperValue = {};
  _.each(withPropertyNames, function (dbpediaPropObject, dbpediaPropCode) {

    // property object is not an object ??? => error
    if (!_.isObject(dbpediaPropObject)) return $$.log.error("Could not figure out value, dbpedia property doesn't seem to be an object: ", dbpediaPropObject);

    // make up array of objects for getting values
    if (_.isArray(dbpediaPropObject)) {
      //  is languages array
      if (
        dbpediaPropObject[0] &&
        dbpediaPropObject[0].lang
      ) var arrayOfValueObjects = [extractPropertyValueFromLanguagesArray(dbpediaPropObject, desiredLanguage)]
      // is other type of array
      else var arrayOfValueObjects = dbpediaPropObject;
    }
    else var arrayOfValueObjects = [dbpediaPropObject];

    // extract values
    withProperValue[dbpediaPropCode] = $$.compact(
      _.map(arrayOfValueObjects, function (valueObject) {
        if (valueObject.type === "literal") return valueObject.value
        else if (valueObject.type === "uri") {
          var processedUriToReturn = convertUriToPrefix(valueObject.value, true, true) || valueObject.value;
          if (_.isString(processedUriToReturn)) processedUriToReturn = processedUriToReturn.replace(/_/g, " ")
          return processedUriToReturn;
        }
        else $$.log.error("Could not figure out the type of value (type is neither 'literal' nor 'uri'): ", valueObject);
      }),
      [undefined, null, NaN, ""]
    );

    // if array has only one entry, convert it to simple value
    if (withProperValue[dbpediaPropCode].length === 1) withProperValue[dbpediaPropCode] = withProperValue[dbpediaPropCode][0];

  });

  // RETURN RESULTING OBJECT
  return withProperValue;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET ASKED FIELDS, PROCESS THEM, STRUCTURE THEM AS ASKED
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function selectOrganizeProcessFields (interpretedDBpediaResponse, fieldsToExtract) {

  var result = {};

  _.each(fieldsToExtract, function (keyOpts) {
    if (keyOpts.process) var value = keyOpts.process(interpretedDBpediaResponse[keyOpts.dbpediaKey])
    else var value = interpretedDBpediaResponse[keyOpts.dbpediaKey];
    if (!_.isUndefined(value)) result[keyOpts.localKey] = value;
  });

  return result;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  see wikipediaFetcher/index.js for API
*/
module.exports = function (urlOrArticleName, fieldsToExtract, lang) {

  //
  //                              MAKE DBPEDIA URL

  if (urlOrArticleName.indexOf("wikipedia") != -1) {
    var parts = urlOrArticleName.split('/');
    var articleCode = parts[parts.length-1];
  }
  else var articleCode = urlOrArticleName.replace(/ /g, "_");
  var dbpediaUrl = "http://dbpedia.org/resource/" + articleCode;

  //
  //                              MAKE REQUEST AND PROCESS IT

  return $.ajax({
    url: "https://dbpedia.org/sparql/",
    data: {
      query: "DESCRIBE <"+ dbpediaUrl +">",
      format: 'application/rdf+json', // 'application/x-json+ld'
    },
    dataType: "json",
  }).then(function (response) {
    return interpretDBpediaResponse(response[dbpediaUrl], lang);
  }).then(function (interpretedDBpediaResponse) {
    return selectOrganizeProcessFields(interpretedDBpediaResponse, fieldsToExtract);
  }).catch(function () {
    $$.log.error("Error trying to fetch contents for '"+ urlOrArticleName +"' wikipedia article.");
  });

  //                              ¬
  //

};
