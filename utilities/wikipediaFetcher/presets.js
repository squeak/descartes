
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FILM
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  film: {

    //
    //                              LANGUAGE TO FETCH CONTENTS IN

    lang: "en",

    //
    //                              FIELDS TO FETCH

    fields: [
      {
        localKey: "actors",
        dbpediaKey: "dbo:starring",
      },
      {
        localKey: "director",
        dbpediaKey: "dbo:director",
        process: function (value) {
          if (!_.isArray(value)) return [value]
          else return value;
        },
      },
      {
        localKey: "country",
        dbpediaKey: "dbp:country",
        process: function (value) {
          if (!_.isArray(value)) return [value]
          else return value;
        },
      },
      {
        localKey: "title",
        dbpediaKey: "foaf:name",
      },
      {
        localKey: "year",
        dbpediaKey: "dc:subject",
        process: function (value) {
          // add support for years (getting it from related subjects)
          var year = _.find(value, function (d) { return d.match(/Category:\d\d\d\d\sfilms/); });
          if (year) return year.match(/\d\d\d\d/)[0];
        },
      },
      {
        localKey: "music",
        dbpediaKey: "dbp:music",
      },
    ],

    //
    //                              POST PROCESS RESULT

    postProcess: function (result) {

      // in case actors, director or music are strings and not array
      _.each(["actors", "director", "music", "country"], function (key) {
        if (_.isString(result[key])) result[key] = [result[key]];
      });

      // fill directorFamilyName field
      if (result.director) {
        result.directorFamilyName = _.map(_.compact(result.director), function (directorName) {
          return directorName.split(/\s/g)[1];
        });
        // // choose first word if asian
        // if (d.country == "China" || d.country == "Taiwan" || d.country == "South Korea" || d.country == "Japan" || d.country == "Viet Nam")
        // directorFamilyName = directorFamilyName[0]
        // // else choose second word
        // else directorFamilyName = directorFamilyName[1];
        // fill directorFamilyName input
      };

      return result;

    },

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
