var _ = require("underscore");
var $$ = require("squeak");
var async = require("async");
var pouchDatabases = require("./dbs");

/**
  DESCRIPTION: check if an object matches the given criterias
  ARGUMENTS: (
    !entry « the entry to test »,
    !selector « the mango query selector object »
  )
  RETURN: <boolean>
*/
var mangoCompare = require("pouchdb-selector-core").matchesSelector;

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var pouchEntries = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET ALL DOCS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get entries from the given pouch database
    ARGUMENTS: (
      !dbName <string>,
      ?filter: <object>,
      !callback <function(<couchEntry[]>)>,
    )
    RETURN: <void>
  */
  fetchAll: function (dbName, filter, callback) {

    // fetch entries
    pouchDatabases.get(dbName).allDocs({
      include_docs: true,
      attachments: true,
    }).then(function (result) {

      // get list of entries from result
      var dbEntries = _.map(result.rows, function (doc) { return doc.doc; });

      // optional filter
      if (filter) dbEntries = _.filter(dbEntries, function (entry) { return mangoCompare(entry, filter) });

      // callback
      callback(dbEntries);

    }).catch(function (err) {
      $$.log.error(err);
      callback([]);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET ONE DOC
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get an entry in a database
    ARGUMENTS: (
      dbName <string>,
      docId <string>,
      callback <function(<couchEntry|undefined>)> « if no document or fail, will just return undefined »,
    )
    RETURN: <void>
  */
  get: function (dbName, docId, callback) {

    pouchDatabases.get(dbName).get(docId, { attachments: true, }).then(function (entry) {
      callback(entry);
    }).catch(function (err) {
      $$.log.error(err);
      callback(undefined);
    });

  },

  /**
    DESCRIPTION: get one or multiple entries in a database (each with an individual request to the database)
    ARGUMENTS: (
      dbName <string>,
      docIdOrDocIds <string|string[]>,
      callback <function(<couchEntry[]>)> « if no document or fail, the array will be empty »,
    )
    RETURN: <void>
  */
  gets: function (dbName, docIdOrDocIds, callback) {

    var docIds = _.isArray(docIdOrDocIds) ? docIdOrDocIds : [docIdOrDocIds];

    async.mapSeries(docIds, function (docId, next) {
      pouchEntries.get(dbName, docId, function (entry) {
        next(null, entry);
      });
    }, function (err, entries) {
      if (err) $$.log.error("[descartes] Error getting multiple entries.", err);
      callback(_.compact(entries));
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FIND ONE OR MULTIPLE ENTRIES IN A DATABASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: find some entries from a given database, passing a mango like filter
    ARGUMENTS: (
      !dbName <string>,
      !selector: <mangoSelector>,
      !callback <function(<couchEntry[]>)>,
    )
    RETURN: <void>
  */
  find: function (dbName, selector, callback) {
    pouchDatabases.get(dbName).find({ selector: selector, }).then(function (entries) {
      callback(entries.docs);
    }).catch(function (err) {
      $$.log.error(err);
      callback(undefined);
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = pouchEntries;
