var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/url");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var pouchAttachments = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET LIST OF ATTACHED IMAGES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      get the list of attachments urls specified at the given key
      supports single and multiple attachments
    ARGUMENTS: (
      !entry <couchEntry>,
      !deepKey <string>,
    )
    RETURN: <{
      title: <string>,
      url <string>,
    }[]> « list of base64 data urls of images, and their titles if one is defined »
  */
  getAttachedImages: function (entry, deepKey) {

    var fileObjects = $$.getValue(entry, deepKey);
    // add support for single file inputs
    if ($$.isObjectLiteral(fileObjects)) fileObjects = [fileObjects]

    // if not files return an empty array
    if (!fileObjects || !fileObjects.length) return [];

    // return clean list of urls
    return _.chain(fileObjects)
      // remove any undefined value in the array
      .compact()
      // make base64 urls
      .map(function (fileObject) {

        try {
          return {
            title: fileObject.title,
            url: pouchAttachments.getUrl(entry, fileObject.attachmentId, true),
          };
        }
        catch (e) {
          if (e == "need_fetch") $$.log.error("[descartes] Failed to get image url for fileObject: ", fileObject, e)
          else $$.log.detailedError("[descartes]", "Unexpected error trying to fetch image from attachment.", e);
        };

      })
      // remove any empty value resulting from error in getting image url
      .compact()
      .value()
    ;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET URL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      get base64 url of an image attached to the passed entry
      ⚠️ this method is synchronous, but if you're not sure your entry has it's attachment fetched, use asynchronous fetchUrl instead
    ARGUMENTS: (
      !entry <couchEntry>,
      !deepkey <string> « where the file object is located (= where to find the id of the attachemnt) »,
      ?deepKeyIsAttachmentId <boolean> « if this is true, will not look for the attachment with deepKey, but will consider deepKey to be the attachment id »,
    )
    RETURN: <string|undefined> « base64 url »
  */
  getUrl: function (entry, deepKey, deepKeyIsAttachmentId) {

    if (deepKeyIsAttachmentId) var imageId = deepKey
    else var imageId = $$.getValue(entry, deepKey +".attachmentId");

    if (entry._attachments && entry._attachments[imageId]) {

      // attachment file
      var file = entry._attachments[imageId];
      if (!file.data) throw "need_fetch"
      else return $$.url.makeBase64(file.data, file.content_type);

    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FETCH URL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get base64 url of an image attached to the passed entry model
    ARGUMENTS: (
      !model <backboneModel>,
      !deepKey <string> « where the file object is located (= where to find the id of the attachemnt) »,
      callback <function(resultUrl <string>)>,
    )
    RETURN: <void>
  */
  fetchUrl: function (model, deepKey, callback) {

    // fetch entry (or just return it if it has no attachments)
    model.fetchWithAttachments(function (fetchedEntry) {
      // callback attachment base 64 url
      if (callback) callback(pouchAttachments.getUrl(fetchedEntry, deepKey));
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = pouchAttachments;
