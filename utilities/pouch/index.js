
module.exports = {
  attachments: require("./attachments"),
  dbs: require("./dbs"),
  entries: require("./entries"),
};
