var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
require("squeak/extension/string");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VALUES EXIST
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: test if one or multiple values are present in the given entry
    ARGUMENTS: (
      entry <couchEntry> « the entry in which to search for the given values »,
      deepKeys <string[]> « the deep keys that should have a value present »,
      andor <"&&"|"||"> « if "&&" is passed, all keys must have a value, otherwise one of them is enough »,
    )
    RETURN: <boolean>
  */
  valuesExist: function (entry, deepKeys, andor) {

    // get all values to check
    var values = _.map(deepKeys, function (deepKey) { return $$.getValue(entry, deepKey); });
    var onlyDefinedValues = $$.array.compact(values);

    // return condition result, all values are present, or at least one
    if (andor === "&&") return onlyDefinedValues.length === values.length
    else return onlyDefinedValues.length;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VALUES MATCH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: test if one or multiple values are matching the given regexp in the given entry
    ARGUMENTS: (
      entry <couchEntry> « the entry in which to search for the given values »,
      deepKeysAndMatcher <{ [deepKey<string>]: matcher<regexp> }> « the deep keys and regexp the value at this key should match in the entry »,
      andor <"&&"|"||"> « if "&&" is passed, all keys must match, otherwise one of them is enough »,
    )
    RETURN: <boolean>
  */
  valuesMatch: function (entry, deepKeysAndMatcher, andor) {

    // get all values to check
    var matchingValues = _.filter(deepKeysAndMatcher, function (matcher, deepKey) {
      var valueFoundAsString = $$.string.make($$.getValue(entry, deepKey));
      return valueFoundAsString.match($$.regexp.make(matcher));
    });

    // return condition result, all values are present, or at least one
    if (andor === "&&") return matchingValues.length === _.keys(deepKeysAndMatcher).length
    else return matchingValues.length;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
