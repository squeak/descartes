var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var inputify = require("inputify");
var chordsDatabase = require("./chordsDatabase.json");
var chordsNames = _.keys(chordsDatabase);

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var chords = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SHOW ONE POSITION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display a single chord's fingers positioning
    ARGUMENTS: (
      !$container <yquerjObject>,
      !position <see showChordPositions·chord.positions>,
    )
    RETURN: <void>
  */
  displayOne: function ($container, position) {

    var $chordPosition = $container.div({ class: "single_position", });
    var finalText = ["&nbsp;"];

    //
    //                              FILL INITIAL CMOMENT LINE

    for (var j = 0; j < 6; j++) {
      if (position.frets[j] == -1) finalText[0] += "<b>X</b>&nbsp;"
      else if (position.frets[j] == 0) finalText[0] += "<b>o</b>&nbsp;"
      else finalText[0] += "&nbsp;&nbsp;";
    };

    //
    //                              FILL THE FOUR LINES IN THE CHORD POSITION

    for (var i = 1; i <= 4; i++) {
      // create line
      finalText.push("|");
      // fill with finger position or with space
      for (var j = 0; j < 6; j++) {
        if (position.frets[j] == i) finalText[i] += "<b>"+ position.fingers[j] +"</b>"
        else finalText[i] += "&nbsp;";
        finalText[i] += "|";
      };
    };

    //
    //                              ADD BARRES

    if (position.barres) _.each(position.barres, function (barreIndex) {
      finalText[barreIndex] = finalText[barreIndex].replace(/&nbsp;/g, "-").replace(/\|/g, "-").replace(/^\-/, "|").replace(/\-$/, "|");
    });

    //
    //                              ADD HORIZONTAL LINES TO CHORD POSITION

    finalText.splice(1, 0, "—————————————");
    finalText.push("—————————————");

    //
    //                              ADD BASE FRET

    if (position.baseFret) {

      // figure out number of spaces to add
      var numberOfSpacesToAdd = (position.baseFret +"").length + 3;
      var spacesToAdd = "";
      for (var k = 0; k < numberOfSpacesToAdd; k++) { spacesToAdd +="&nbsp;"; };

      // add base fret or spaces in front of each line
      for (var i = 0; i <= 6; i++) {
        if (i == 2) finalText[i] = position.baseFret +"fr " + finalText[i];
        else finalText[i] = spacesToAdd + finalText[i];
      };

    };

    //
    //                              ADD TEXT TO DOM

    $chordPosition.htmlSanitized(finalText.join("<br>"));

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SHOW CHORD POSITIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display the given chord's possible playing positions
    ARGUMENTS: (
      !$container <yquerjObject>,
      !chord <{
        !key: <string>,
        !suffix: <string>,
        ?positions: <{
          ?baseFret: <integer>,
          ?barres: <integer[]>,
          !frets: <[integer, integer, integer, integer, integer, integer]>
          !fingers: <[integer, integer, integer, integer, integer, integer]>
        }[]>
      }>
    )
    RETURN: <void>
  */
  displayMultiple: function ($container, chord) {

    var $chord = $container.div({ class: "guitar_chords", });
    var $title = $chord.div({
      class: "chord_title",
      text: chord.key + chord.suffix,
    });
    var $positions = $chord.div({ class: "chord_positions", });

    _.each(chord.positions, function (position, positionIndex) {
      chords.displayOne($positions, position);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY DIALOG WITH ASKED CHORDS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display a dialog, movable and resizable, displaying asked chords positions, and allowing to change chords to display
    ARGUMENTS: (
      ?$container: <yquerjObject> « container in which to display dialog »,
      ?askedChords <string[]>,
    )
    RETURN: <void>
  */
  chordsDisplayDialog: function ($container, askedChords) {

    var dialog = uify.dialog({
      $container: $container || $("body"),
      title: "Chords diagrams",
      draggable: true,
      resizable: true,
      disableEnterKeyValidation: true,
      disableEscKeyCancel: true,
      overlayClickExits: false,
      overlay: false,
      buttons: [],
      content: function ($container) {

        //
        //                              CREATE INPUT AND CHORDS CONTAINERS

        var $inputContainer = $container.div({ class: "chords_choice", });
        var $chords = $container.div({ class: "chords_list", });

        //
        //                              CREATE INPUT TO CHOSE CHORDS TO DISPLAY

        inputify({
          $container: $inputContainer,
          type: "suggest",
          name: "chords",
          help: "list of chords to display",
          labelLayout: "hidden",
          choices: chordsNames,
          value: askedChords,
          changed: function (askedChords) {

            // remove previous chords
            $chords.empty();

            // display chords
            _.each(askedChords, function (chordName) {
              chords.displayMultiple($chords, chordsDatabase[chordName]);
            });

          },
        });

        //                              ¬
        //

      },
    });

    // make sure dialog hasn't fixed positioning, otherwise it will be displayed everywhere, including on dbs in which it shouldn't
    dialog.$overlay.css("position", "absolute");
    dialog.$dialog.css("position", "absolute");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = chords;
