var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
require("squeak/extension/date");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAPPINGS AND CORRESPONDANCES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var priorityOrderingMap = {
  high: "aaa",
  normal: "bbb",
  low: "ccc",
  undefined: "zzz",
  "": "zzz",
};

var todo2siblingCorrespondances = {
  toprocess: ["todo", "totry", "toflex", "todont"],
  todo: ["toprocess", "totry", "toflex", "todont"],
  totry: ["toprocess", "todo", "toflex", "todont"],
  toflex: ["toprocess", "todo", "totry", "todont"],
  todont: ["toprocess", "todo", "totry", "toflex"],
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var subjectAndMonitoring = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAPS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  frequencyOrderingMap: {
    multidaily: "aa",
    daily: "bb",
    otherdaily: "cc",
    weekly: "dd",
    monthly: "ee",
    quarterly: "ff",
    yearly: "gg",
    occasional: "hh",
    undefined: "zz",
    "": "zz",
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHECK IF DB RECURRING EVENT HAS ALREADY NOTIFIED TODAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      returns true if this function has already been ran today
      ⚠️ new days start at 5AM
    ARGUMENTS: (
      dbConfig <couchEntry>,
      methodIndex <integer>,
    )
    RETURN: <boolean>
  */
  sortSubjectsByFrequency: function (subjectsEntries) {

    return _.sortBy(subjectsEntries, function (entry) {
      return subjectAndMonitoring.frequencyOrderingMap[entry.frequency] +"---"+ priorityOrderingMap[entry.priority] +"---"+ entry.context +"/"+ entry.name;
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DURATION CALCULATOR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: convert a duration string into a number of minutes
    ARGUMENTS: (
      duration <string> «
        example of strings that can be understood: "1day", "2hours", "3h", "10min-3h", "~2h", "10min+"
        ~ and + are simply ignored
        if a min and max durations are specified (e.g. "10h-12h") the average will be used (in this case for example: 11h)
      »,
    )
    RETURN: <number>
  */
  durationCalculator: function (duration) {
    if (!_.isString(duration)) return 0;

    // remove not so important imprecisions
    duration = duration.replace(/\+$/, "").replace(/^\~/, "");
    // support "flex" duration
    if (duration === "?") return 0;

    // if duration span, use average of boths
    if (duration.match("-")) {
      var splitDuration = duration.split("-");
      return $$.round((subjectAndMonitoring.durationCalculator(splitDuration[0]) + subjectAndMonitoring.durationCalculator(splitDuration[1])) / 2, 1);
    };

    // figure out duration number
    var durationNumber = duration.replace(/[^\d]*$/, "");
    var durationText = duration.replace(/^[\d]*/, "");
    if (!durationNumber) $$.log.error("descartes: Failed to find duration number for duration: ", duration);
    if (!durationText) $$.log.error("descartes: Failed to find duration text for duration: ", duration);
    return $$.date.moment.duration(durationNumber, durationText).asMinutes();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MOVING SUBJECTS TO SIBLINGS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: refresh repartitions of subjects in lists
    ARGUMENTS: (
      inputified « should be the main postified inputified containing full entry »,
    )
    RETURN: <void>
  */
  dispatchSubjectsToMonitor: function (inputified) {
    var parentValue = inputified.get();

    var finalValues = {};
    // fill with the ones not to move
    _.each(_.keys(todo2siblingCorrespondances), function (key) {
      finalValues[key] = _.where(parentValue[key], { todo: key }) || [];
    });
    // fill with the ones to move
    _.each(todo2siblingCorrespondances, function (otherKeys, key) {
      _.each(otherKeys, function (otherKey) {
        finalValues[otherKey] = $$.array.merge(
          finalValues[otherKey],
          _.where(parentValue[key], { todo: otherKey })
        );
      });
    });

    // add new values to old entry (sorted)
    _.each(finalValues, function (value, key) {
      parentValue[key] = subjectAndMonitoring.sortSubjectsByFrequency(value);
    });

    // set new entry value
    inputified.set(parentValue);

  },

  /**
    DESCRIPTION: same as dispatchSubjectsToMonitor but displaying a spinner while doing it
    ARGUMENTS: (
      postified,
      callback <function(ø)>
    )
    RETURN: <void>
  */
  dispatchSubjectsToMonitorWithSpinner: function (postified, callback) {
    var spinner = uify.spinner({
      $container: postified.$window,
      overlay: true,
    });
    setTimeout(function () {
      subjectAndMonitoring.dispatchSubjectsToMonitor(postified.inputified);
      spinner.destroy();
      if (_.isFunction(callback)) callback();
    }, 10);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  COPY LIST OF TODOS TO CLIPBOARD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: copy to clipboard todo and totry entries in a text-friendly style
    ARGUMENTS: (
      !entry <couchEntry>,
      !collection <backboneCollection>,
    )
    RETURN: <void>
  */
  copyTodosToClipboard: function (entry, collection) {
    var resultArray = [];

    function fillResultArray (what) {

      // add category title
      resultArray.push(what.toUpperCase());

      _.each(entry[what], function (subjObj) {
        if (subjObj.status !== "done") {
          var subjectModel = collection.get(subjObj.subject);
          if (!subjectModel) return $$.log.error("Failed to find subject model for the following monitored subject: ", subjObj);
          var todoText = "- ";
          var point = subjectModel.get("priority") === "high" ? "!" : ".";

          // li . s . ts
          var lists = subjectModel.get("list");
          if (lists && lists.length) todoText += point +" "+ lists.join(" "+ point +" ");
          // [context/name]
          todoText += " ["+ subjectModel.get("context") +"/"+ subjectModel.get("name") +"]";
          // (durationPlane ?frequency)
          todoText += " ("+ (subjObj.durationPlan || "") + (_.indexOf(["multidaily", "daily", "otherdaily"], subjObj.frequency) !== -1 ? " "+ subjObj.frequency : "") +")";

          // add to resultarray
          resultArray.push(todoText);

        };
      });

      // jump line after this category
      resultArray.push("");

    };

    // add "todo" and "totry" lists
    fillResultArray("todo");
    fillResultArray("totry");

    // copy text to clipboard
    $$.copyToClipboard(resultArray.join("\n"));
    uify.toast("Copied todo list to clipboard.");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = subjectAndMonitoring;
