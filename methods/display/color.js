var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/color");
require("squeak/extension/string");
var customMethod = require("../../customMethod");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  ARGUMENTS: (
    entry <couchEntry>,
    typeConfig <couchEntry> « the full configuration object for this type of entry »,
  ){@this=<backboneModel>}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  JUST USE THE GIVEN COLOR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  result: {
    description: "Just use this color.",
    arguments: [
      {
        label: "color",
        help: "Choose here the color you want to use.",
        type: "color",
      },
    ],
    method: function (zeusArgs, entry, typeConfig) {
      return zeusArgs[0];
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  USE COLOR IN ENTRY AT A SPECIFIC KEY OR FALLBACK ON THE GIVEN COLOR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valueOrResult: {
    description: "Use the color set in the entry at the given key, or if there is no color set there, fallback on the one given here.",
    arguments: [
      {
        label: "deepKey",
        help: "Choose here the key where to find the color in your entry.",
        type: "normal",
      },
      {
        label: "color",
        help: "Choose here the fallback color you want to use.",
        type: "color",
      },
    ],
    method: function (zeusArgs, entry, typeConfig) {
      var colorInEntry = $$.getValue(entry, zeusArgs[0]);
      var fallbackColor = zeusArgs[1];
      return colorInEntry || fallbackColor;
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  USE A RANDOM COLOR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  random: {
    description: "Choose a random color.",
    arguments: [
      {
        label: "opacity",
        help: "Choose the level of opacity for the color. If you choose 0, the opacity will be randomized.",
        type: "slider",
        defaultValue: 1,
      },
    ],
    method: function (zeusArgs, entry, typeConfig) {
      var opacity = zeusArgs[0];
      return $$.random.color(opacity ? opacity : undefined);
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VALUE-COLOR PAIRS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valueToColor: {
    description: "Specify a list of possible values, and the colors that should correspond to them.",
    arguments: [
      {
        label: "deepKey",
        type: "normal",
        help: "Deep key where is stored the value to get.",
      },
      {
        label: "valueColorPairs",
        type: "object",
        help: "A list of value-color pairs, defining which color to use for each specific value.",
        object: {
          model: { type: "color", },
        },
      },
      {
        label: "backupColor",
        help: "The color to use if the value doesn't match any in the previous list.",
        type: "color",
      },
    ],
    method: function (zeusArgs, entry, typeConfig) {
      var valueColorPairs = zeusArgs[1] || {};
      return valueColorPairs[$$.getValue(entry, zeusArgs[0])] || zeusArgs[2];
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE COLOR FROM STRING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  randomColorFromAnyValue: {
    description: "Make up a color from the value present at the given key.",
    arguments: [
      {
        label: "deepKey",
        type: "normal",
        help: "Deep key where is stored the value to use to generate the color (any kind of value can be stored at this key, it will be processed to generate a \"random\" color).",
      },
      {
        label: "availableColors",
        type: "array",
        help: "A list of colors from which to choose from (if you don't want to have totally random colors generated).",
        object: {
          model: { type: "color", },
        },
      },
      {
        label: "defaultColor",
        type: "color",
        help: "The fallback color to use if no value is defined at the given key.",
      },
    ],
    method: function (zeusArgs, entry, typeConfig) {
      return $$.color.anyString2HexColor(
        $$.string.make($$.getValue(entry, zeusArgs[0])),
        zeusArgs[1],
        zeusArgs[2]
      );
    },
  },

  // //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  // //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  // //                                                  MAKE COLOR FROM STRING
  // //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //
  // randomColorFromAnyValueWithMatching: {
  //   description: "Make up a color from a part of the value present at the given key.",
  //   arguments: [
  //     {
  //       label: "deepKey",
  //       type: "normal",
  //       help: "Deep key where is stored the value to use to generate the color (any kind of value can be stored at this key, it will be processed to generate a \"random\" color).",
  //     },
  //     {
  //       label: "matchingRegexp",
  //       type: "normal",
  //       help: 'You may define a regexp, using the following syntax: <span class="descartes-monospace">/stringMatcher/</span>, you may also pass regexp flags, for example <span class="descartes-monospace">/stringmatcher/i</span>.',
  //     },
  //     {
  //       label: "availableColors",
  //       type: "array",
  //       help: "A list of colors from which to choose from (if you don't want to have totally random colors generated).",
  //       object: {
  //         model: { type: "color", },
  //       },
  //     },
  //     {
  //       label: "defaultColor",
  //       type: "color",
  //       help: "The fallback color to use if no value is defined at the given key.",
  //     },    ],
  //   method: function (zeusArgs, entry, typeConfig) {
  //     return $$.color.anyString2HexColor(
  //       $$.match(
  //         $$.string.make($$.getValue(entry, zeusArgs[0])),
  //         $$.regexp.make(zeusArgs[1])
  //       ),
  //       zeusArgs[2],
  //       zeusArgs[3]
  //     );
  //   },
  // },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("display.color"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
