
module.exports = {
  additional: require("./additional"),
  bodyHeader: require("./bodyHeader"),
  buttonClick: require("./buttonClick"),
  buttonCondition: require("./buttonCondition"),
  color: require("./color"),
  geoKey: require("./geoKey"),
  icon: require("./icon"),
  image: require("./image"),
  title: require("./title"),
};
