
module.exports = {
  changed: require("./changed"),
  moved: require("./moved"),
  removed: require("./removed"),
  saved: require("./saved"),
};
