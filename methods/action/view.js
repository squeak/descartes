var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var uify = require("uify");
var customMethod = require("../../customMethod");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  ARGUMENTS: (
    model <backboneModel> « model of the entry to view »,
    typeConfig <tablify·entryType> « type configuration of the entry to edit »,
    addToHash <function(ø)> « will add entry id to hash when executed (be sure to run this in the method code if you want dato to remember opened entries) »,
    removeFromHash <function(ø)> « will remove entry id from hash when executed (if you ran addToHash callback, be sure to run this when entry is closed) »,
    e <event> « the click event if this was triggered by the user »,
  ){@this=App}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              OPEN FOLDER

  folder: {
    description: "Open this entry as a subfolder of this database.",
    arguments: null,
    method: function (zeusArgs, model, typeConfig, addToHash, removeFromHash, e) {
      var App = this;
      App.openPath(model);
      App.select();
    },
  },

  //
  //                              OPEN EDITION PAGE

  edit: {
    description: "Open the edition window of this entry.",
    arguments: null,
    method: function (zeusArgs, model, typeConfig, addToHash, removeFromHash, e) {
      var App = this;
      App.openEntryEditor(model, false, "v");
    },
  },

  //
  //                              OPEN CUSTOMLY SPECIFIED URL

  openUrl: {
    description: "Open the given url.",
    arguments: [
      {
        label: "pageUrl",
        help: "The url of the page you want to open. Anything you pass in between %%something%% will get the value at the corresponding deepKey in the entry.\nFor example /my-nice-url/%%author.name%%/%%title%% will be turned into something like /my-nice-url/Hugo/93",
        type: "normal",
      },
    ],
    method: function (zeusArgs, model, typeConfig, addToHash, removeFromHash, e) {
      var App = this;
      var pageUrl = zeusArgs[0] || "";

      var finalUrl = $$.string.interpolate({
        string: pageUrl,
        entry: model.attributes,
        style: "%%",
      });

      if (finalUrl) $$.open(finalUrl, e && e.ctrlKey)
      else alert("Could not figure out the url you want to open.");

    },
  },

  //
  //                              COPY ANY GIVEN VALUE OR INTERPOLATED STRING

  copyValueToClipboard: {
    description: "Copy the specified value to clipboard.",
    arguments: [
      {
        label: "pageUrl",
        help: "The value you want to copy to clipboard. Anything you pass in between %%something%% will get the value at the corresponding deepKey in the entry.\nFor example %%author.name%%/%%title%% will be turned into something like Hugo/93",
        type: "normal",
      },
    ],
    method: function (zeusArgs, model, typeConfig, addToHash, removeFromHash, e) {
      var App = this;

      var finalValue = $$.string.interpolate({
        string: zeusArgs[0] || "",
        entry: model.attributes,
        style: "%%",
      });

      if (finalValue) {
        $$.copyToClipboard(_.isObject(finalValue) ? $$.json.pretty(finalValue) : finalValue);
        uify.toast("Copied '"+ finalValue +"' to clipboard.");
      }
      else uify.toast.error("Could not figure out the value you want to copy.");

    },
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("action.view"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
