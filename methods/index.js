
/**
  DESCRIPTION:
    all the methods listed below and the corresponding files are accessible within any grape parameter
    to use a method listed here in a grape, just set the value of this grape's key to a string starting with "zeus+" followed with the deepKey of the method you want to use
    if you need to pass arguments to this method, use the following syntax: "zeus+deep.key(arg1, arg2)"
*/
module.exports = {

  action: require("./action"),
  db: require("./db"),
  display: require("./display"),
  edify: require("./edify"),
  event: require("./event"),
  graphify: require("./graphify"),
  inputify: require("./inputify"),
  tablify: require("./tablify"),
  viewify: require("./viewify"),

};
