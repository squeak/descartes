var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/date");
require("squeak/extension/string");
var getDatabaseEntries = require("../../utilities/getDatabaseEntries");
var subjectAndMonitoring = require("../../utilities/subjectAndMonitoring");
var presarg = require("../../presetArguments");
var customMethod = require("../../customMethod");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  ARGUMENTS: ( ø ){@this=inputified}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TODAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  today: {
    description: "Today's date.",
    arguments: null,
    method: function (zeusArgs) {
      return $$.date.day();
    },
    // arguments: [
    //   {
    //     label: "isRange",
    //     type: "boolean",
    //     help: "Set this to true if you want to generate a date range, false to set just a single date.",
    //   },
    // ],
    // method: function (zeusArgs) {
    //   return $$.date.day() +(zeusArgs[0] ? "#" : "");
    // },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TODAY MINUS 4 HOURS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  todayLate: {
    description: "Four hours ago's date.",
    arguments: null,
    method: function (zeusArgs) {
      return $$.date.make().subtract(4, "hours").format("YYYY-MM-DD");
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  RETURN THE GIVEN VALUE, ONLY IF THE ENTRY IS NEW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  ifNew: {
    description: "Will return the specified value only if the entry is new.",
    arguments: [
      {
        label: "value",
        type: "any",
        help: "The value to return.",
      },
    ],
    method: function (zeusArgs) {
      var inputified = this;
      var value = zeusArgs[0];
      if (inputified.storageRecursive("isNewEntry")) return value;
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FILL FROM VALUES IN OTHER ENTRY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  fillFromValuesInOtherEntry: {
    description: "Let's you fill the value from another entry's values, using simple correspondances/templating.",
    arguments: [
      presarg.database.mangoFilter,
      presarg.interpolate.string,
      {
        label: "isList",
        type: "boolean",
        help: "Set this to true to interpolate the whole list of matching entries and return an array, otherwise only the first matching entry will be interpolated.",
      },
    ],
    method: function (zeusArgs) {
      var inputified = this;
      var Collection = inputified.storageRecursive("postified.storage.collection");

      // interpolate entry to figure out value to return
      function interpolate (entry) {
        var result = $$.string.interpolate({
          complexExpressions: true,
          string: zeusArgs[1],
          entry: entry,
        });
        // if value can be parsed to js, do it
        try { return JSON.parse(result); }
        catch (e) { return result; };
      };

      // get asked db entries
      var entries = getDatabaseEntries(Collection, zeusArgs[0]);

      // interpolate one or all entries
      if (zeusArgs[2]) return _.map(entries, interpolate)
      else return interpolate(entries[0]);

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SIBLING VALUE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  siblingValue: {
    description: "Fill the value from a sibling input.",
    arguments: [
      {
        label: "key",
        type: "normal",
        help: "The key of the sibling input to fill value from.",
      },
      {
        label: "matchingRegexp",
        type: "any",
        help: "If you want to match only a part of the sibling input, you may pass here a regexp, like for example: /^\d*/",
      },
    ],
    method: function (zeusArgs) {
      var inputified = this;
      var siblingValue = inputified.getSiblingValue(zeusArgs[0]);
      if (zeusArgs[1]) return $$.match(siblingValue, $$.regexp.make(zeusArgs[1]))
      else return siblingValue;
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SIBLING VALUE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  subvalueInParentInput: {
    description: "Fill the value from a subvalue in parent input.",
    arguments: [
      {
        label: "key",
        type: "normal",
        help: "The key of the subvalue to get in parent input.",
      },
    ],
    method: function (zeusArgs) {
      var inputified = this;
      var parentInputified = inputified.storage("parentInputified");
      if (!parentInputified) return $$.log.error("[descartes] Failed to figure out parent inputified for input:", inputified);
      var parentInputifiedValue = parentInputified.get();
      if (parentInputifiedValue) return parentInputifiedValue[zeusArgs[0]];
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FILL MONITORING FROM SUBJECTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  fillMonitoringFromSubjects: {
    description: "This custom method is specifically designed for prefilling monitoring entries in subjects-monitoring databases.",
    arguments: [
      {
        label: "timeShouldBe",
        type: "multi",
        help: 'Pass here the values "time" subjects should have to be selected.',
      },
      {
        label: "invertTimeShouldBe",
        type: "boolean",
        help: 'Set this to true to invert behaviour of "timeShouldBe", turning it into a "timeShouldNotBe"',
      },
    ],
    method: function (zeusArgs) {
      var inputified = this;
      var Collection = inputified.storageRecursive("postified.storage.collection");

      // get db subject entries
      var subjectsEntries = getDatabaseEntries(Collection, { type: "subject", });

      // filter and sort subject entries
      var subjectsEntriesWithoutIgnored = _.filter(subjectsEntries, function (entry) {
        if (zeusArgs[1]) return _.indexOf(zeusArgs[0], entry.organizing) === -1
        else return _.indexOf(zeusArgs[0], entry.organizing) !== -1;
      });
      var sortedEntries = subjectAndMonitoring.sortSubjectsByFrequency(subjectsEntriesWithoutIgnored);

      // return prefilled monitoring values
      return _.map(sortedEntries, function (entry) {
        return {
          subject: entry._id,
          // name: entry.context +"/"+ entry.name,
          todo: inputified.name,
          priority: entry.priority,
          frequency: entry.frequency,
          durationPlan: entry.duration,
           // + (_.indexOf(["multidaily", "daily", "otherdaily"], entry.frequency) !== -1 ? " / day" : ""),
        };
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("inputify.value"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
