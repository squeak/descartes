var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var pouchEntries = require("../../utilities/pouch/entries");
var presarg = require("../../presetArguments");
var multinterpolate = require("../../utilities/multinterpolate");
var getDatabaseEntries = require("../../utilities/getDatabaseEntries");
var customMethod = require("../../customMethod");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE CHOICES LIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function callbackChoicesList (entries, deepKey, callback, display) {

  // make custom string from entry values
  if (display) {
    var choices = _.map(entries, function (entry) {
      var label = $$.string.interpolate({
        string: display.string,
        fallback: display.fallback,
        missing: display.missing,
        entry: entry,
        complexExpressions: true,
      });
      return {
        _isChoice: true,
        label: label,
        value: deepKey ? $$.getValue(entry, deepKey) : entry,
        search: label,
      };
    });
    choices = _.uniq(choices, false, function (choiceObject) { return choiceObject.label; });
  }

  // get values raw
  else var choices = _.chain($$.pluck(entries, deepKey)).flatten().compact().uniq().value();

  // callback list of choices
  callback(choices);

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  ARGUMENTS: ( !callback(choices<inputified·options.choices>) ){ @this=inputified }
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SUGGEST OTHER ENTRIES VALUES AT SAME KEY IN DATABASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valuesInThisDatabaseEntries_sameKey: {
    description: "Suggest values available at this key in entries in this database.",
    arguments: [
      presarg.database.mangoFilter,
    ],
    method: function (zeusArgs, callback) {
      var inputified = this;
      var Collection = inputified.storageRecursive("postified.storage.collection");

      // get asked db entries
      var entries = getDatabaseEntries(Collection, zeusArgs[0]);

      // callback list of choices
      callbackChoicesList(entries, inputified.deepKey, callback);

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SUGGEST OTHER ENTRIES VALUES AT SAME KEY IN DATABASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valuesInThisDatabaseEntries: {
    description: "Suggest values at the given key in entries of this database.",
    arguments: [
      presarg.database.mangoFilter,
      presarg.deepKey.forChoices,
      presarg.interpolate.string,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, callback) {
      var inputified = this;
      var Collection = inputified.storageRecursive("postified.storage.collection");

      // get asked db entries
      var entries = getDatabaseEntries(Collection, zeusArgs[0]);

      // callback list of choices
      callbackChoicesList(entries, zeusArgs[1], callback, {
        string: zeusArgs[2],
        fallback: zeusArgs[3],
        missing: zeusArgs[4],
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY OTHER ENTRIES VALUES FROM ANOTHER DATABASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valuesInOtherDatabaseEntries: {
    description: "Suggest values at the given key in entries of another database.",
    arguments: [
      presarg.database.choose,
      presarg.database.filter,
      presarg.deepKey.forChoices,
      presarg.interpolate.string,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, callback) {
      var inputified = this;

      // get asked db entries
      pouchEntries.fetchAll(zeusArgs[0], zeusArgs[1], function (entries) {

        // callback list of choices
        callbackChoicesList(entries, zeusArgs[2], callback, {
          string: zeusArgs[3],
          fallback: zeusArgs[4],
          missing: zeusArgs[5],
        });

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY VALUES IN AN ENTRY FROM ANOTHER DATABASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valuesInOtherDatabaseEntry: {
    description: "Suggest values listed at the given key in another database entry (if multiple entries are matched, arrays of suggestions will be merged).",
    arguments: [
      presarg.database.choose,
      presarg.database.filter,
      {
        type: "normal",
        label: "Deep key containing suggestions:",
        labelLayout: "stacked",
        help: "What deep key to find suggestion values in.",
        mandatory: true,
      },
      {
        type: "normal",
        label: "Deep key containing value (relative to suggestion object):",
        labelLayout: "stacked",
        help: "Deep key where value to use is stored in suggestions, if suggestion is already the full value to keep, leave this empty",
      },
      presarg.interpolate.string,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, callback) {
      var inputified = this;

      // get asked db entries
      pouchEntries.fetchAll(zeusArgs[0], zeusArgs[1], function (entries) {

        // make array of choices (yet to be interpolated)
        let arrayOfStuffToInterpolate = $$.array.merge.apply(this, $$.pluck(entries, zeusArgs[2]));

        // callback list of choices
        callbackChoicesList(arrayOfStuffToInterpolate, zeusArgs[3], callback, {
          string: zeusArgs[4],
          fallback: zeusArgs[5],
          missing: zeusArgs[6],
        });

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY VALUES ALREADY PRESENT IN THE CURRENTLY EDITED ENTRY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valuesInCurrentlyEditedEntry: {
    description: "Suggest values at the given key in the currently edited entry.",
    arguments: [
      {
        type: "multi",
        label: "Deep key(s) to search value(s) in:",
        labelLayout: "stacked",
        help: "Deep key(s) of the value(s) you want to extract and propose as suggestions.",
        mandatory: true,
      },
    ],
    method: function (zeusArgs, callback) {
      var inputified = this;

      var rootInputified = inputified.storageRecursive("rootInputified");
      var valuesToSuggest = [];

      // EXTRACT VALUES FROM INPUTIFIED
      if (rootInputified && _.isFunction(rootInputified.get)) {

        // get input value preferably from defaultValue, because otherwise
        // while values are being set in object, the rootInputified.get() value changes, making inconsistant list of choices
        var inputValue = rootInputified.options.defaultValue || rootInputified.get();

        // make list of values to suggest
        _.each(zeusArgs[0], function (deepkeyToSearchIn) {
          valuesToSuggest = $$.array.merge(valuesToSuggest, $$.getValue(inputValue, deepkeyToSearchIn));
        });
        
      }
      // COULD NOT FIGURE OUT INPUTIFIED
      else $$.log.error(
        "Could not find the root input of for this postify window.",
        "inputified:", inputified,
        "root inputified:", rootInputified
      );

      // CALLBACK LIST OF CHOICES
      callback(_.uniq(valuesToSuggest));

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET LIST OF CHOICES FROM A DATA ENTRY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valuesInDataEntry: {
    description: "Suggest values stored in a data entry (data entries are creatable in the types database).",
    arguments: [
      {
        type: "normal",
        label: "Name of the data entry to fetch values in:",
        labelLayout: "stacked",
        mandatory: true,
      },
      {
        type: "normal",
        label: "Deepkey where the value is stored in data objects:",
        labelLayout: "stacked",
        help: "Leave this blank if the whole data object is the value to suggest."
      },
      {
        type: "normal",
        label: "A string to interpolate with data object to display as custom label:",
        labelLayout: "stacked",
        help: "If you leave this blank, the value will be used as label.",
      },
    ],
    method: function (zeusArgs, callback) {
      var inputified = this;

      // fetch asked data entry
      pouchEntries.fetchAll("dato", { name: zeusArgs[0], }, function (entries) {

        if (!entries || !entries.length) return $$.log.error("Could not find the data entry named '"+ zeusArgs[0] +"' in types database.", entries)
        else {
          if (entries.length > 1) $$.log.warning("Found more than one entry named '"+ zeusArgs[0] +"' in types database, only the first one has been used to get choices.");
          var choices = _.map(entries[0].data, function (dataObject) {

            // make value for this choice
            var value = zeusArgs[1] ? $$.getValue(dataObject, zeusArgs[1]) : dataObject;
            // make lable for this choice
            var label = zeusArgs[2] ? $$.string.interpolate({
              string: zeusArgs[2],
              entry: dataObject,
              complexExpressions: true,
            }) : value;

            // return choice object
            return {
              _isChoice: true,
              value: value,
              label: label,
              search: label,
            };

          });
          // callback list of choices
          callback(choices);
        };

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("inputify.choices"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
