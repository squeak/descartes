var _ = require("underscore");
var $$ = require("squeak");
var presarg = require("../../presetArguments");
var customMethod = require("../../customMethod");

/**
  DESCRIPTION: check if an object matches the given criterias
  ARGUMENTS: (
    !entry « the entry to test »,
    !selector « the mango query selector object »
  )
  RETURN: <boolean>
*/
var mangoCompare = require("pouchdb-selector-core").matchesSelector;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SIBLING ARGUMENTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var siblingsArgs = [
  {
    label: "siblingKey",
    type: "normal",
    help: "The key of the other input to compare with.",
  },
  {
    label: "siblingValue",
    type: "any",
    help: "The value this input should have for this input to be displayed.",
  },
  {
    label: "valueIsMangoSelector",
    type: "boolean",
    help: "If you want more powerful comparison, you can pass mango selectors as siblingValue. If you do so, set this to true.\nFor documentation on mango selectors check this out: https://docs.mongodb.com/manual/reference/operator/query/",
  },
];

// ⚠ THIS IS IMPORTANT SO THAT siblingsArgs DO NOT CONTAIN THE "key" KEY, OTHERWISE FOR SOME REASON, ARGUMENTS CAN'T BE SAVED TO STRING ⚠
var siblingArgsWithKeys = _.map(siblingsArgs, function (argObject) {
  let clonedArgObject = _.clone(argObject);
  clonedArgObject.key = clonedArgObject.label;
  return clonedArgObject;
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SIBLING VALUE CHECKING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: check if a single sibling's value matched the given predicate
  ARGUMENTS: (
    inputified,
    options <{
      siblingKey: <string>,
      siblingValue: <any|mangoQuery>,
      valueIsMangoSelector: <boolean>,
    }>,
  )
  RETURN: <boolean>
*/
function siblingValue (inputified, options) {
  // get sibling inputified
  if (!inputified.getSibling) return;
  var siblingInputified = inputified.getSibling(options.siblingKey);
  if (!siblingInputified) return;

  // check condition from mango query selector or equality
  if (options.valueIsMangoSelector) return mangoCompare(siblingInputified.get({ objectResult: true, }), { value: options.siblingValue, })
  else return _.isEqual(siblingInputified.get(), options.siblingValue);
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  ARGUMENTS: ( ø ){@this=inputified}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHECK A SIBLING INPUT'S VALUE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  siblingValue: {
    description: "Check a value in a sibling input.",
    arguments: siblingsArgs,
    method: function (zeusArgs) {
      var inputified = this;
      return siblingValue(inputified, {
        siblingKey: zeusArgs[0],
        siblingValue: zeusArgs[1],
        valueIsMangoSelector: zeusArgs[2],
      });
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHECK MULTIPLE SIBLING INPUT'S VALUE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  siblingsValues: {
    description: "Check the values of siblings inputs.",
    arguments: [
      {
        label: "siblingsToCheck",
        type: "array",
        object: {
          sortDisplay: "siblingKey",
          model: {
            inputifyType: "object",
            object: {
              structure: siblingArgsWithKeys,
            },
          },
        },
      },
      presarg.conditions.andor_present,
    ],
    method: function (zeusArgs) {
      var inputified = this;
      if (zeusArgs[1] !== "||" && zeusArgs[1] !== "&&") zeusArgs[1] = "&&";
      for (var i = 0; i < zeusArgs[0].length; i++) {
        var siblingConditionValidated = siblingValue(inputified, zeusArgs[0][i]);
        if (zeusArgs[1] === "||" && siblingConditionValidated) return true
        else if (zeusArgs[1] === "&&" && !siblingConditionValidated) return false;
      }
      if (zeusArgs[1] === "||") return false
      else if (zeusArgs[1] === "&&") return true
      else $$.log.error("descartes:inputify:condition:siblingsValues there is an internal error in this function, you should probably contact the developper");
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("inputify.condition"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
