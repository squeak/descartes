var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var uifyCrop = require("uify/plugin/crop");
var customMethod = require("../../customMethod");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  ARGUMENTS: (
    file <Blob|File> « the file to process »,
    callback <function(blob<Blob|File>)> « called back when file has been processed, pass the result file »,
  ){@this=inputified}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FULL PROMPT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  fullPrompt: {
    description: "Let the user choose all the processing it wants to do to an image (croppings, resizings...)",
    arguments: null,
    method: function (zeusArgs, file, callback) {
      var inputified = this;

      uify.menu({
        title: "What processing do you want to do?",
        buttons: [
          {
            title: "resizing",
            action: "resizePrompt",
          },
          {
            title: "square cropping",
            action: "squareCrop",
          },
          {
            title: "rectangular cropping",
            action: "rectangularCrop",
          },
          {
            title: "non rectangular cropping",
            action: "nonRectangularCrop",
          },
          {
            title: "nothing (more)",
            action: null,
            key: "enter",
          },
        ],
        click: function (buttonOptions) {

          // do asked action, and reprompt for next action
          if (module.exports[buttonOptions.action]) module.exports[buttonOptions.action].method(zeusArgs, file, function (processedFile) {
            module.exports.fullPrompt.method(zeusArgs, processedFile, callback);
          })
          // finished all processings
          else {
            if (buttonOptions.action) $$.log.error("[descartes] Unknown file processing action:", buttonOptions.action);
            else callback(file);
          };

        },

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SQUARE CROP
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  squareCrop: {
    description: "Show an interface to the user giving them the possibility to crop the image as a square.",
    arguments: null,
    method: function (zeusArgs, file, callback) {
      var inputified = this;

      // IS NOT IMAGE, JUST SKIP CROPING
      if (!file.type || !file.type.match(/^image\//)) return callback(file);

      // PROPOSE CROPING
      uifyCrop({
        image: file,
        shape: "rectangular",
        ratio: 1,
        callback: callback,
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  RECTANGULAR CROP
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  rectangularCrop: {
    description: "Show an interface to the user giving them the possibility to crop the image.",
    arguments: null,
    method: function (zeusArgs, file, callback) {
      var inputified = this;

      // IS NOT IMAGE, JUST SKIP CROPING
      if (!file.type || !file.type.match(/^image\//)) return callback(file);

      // PROPOSE CROPING
      uifyCrop({
        image: file,
        shape: "rectangular",
        callback: callback,
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  NON RECTANGULAR CROP
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  nonRectangularCrop: {
    description: "Show an interface to the user giving them the possibility to crop the image by drawing a complex polygon.",
    arguments: null,
    method: function (zeusArgs, file, callback) {
      var inputified = this;

      // IS NOT IMAGE, JUST SKIP CROPING
      if (!file.type || !file.type.match(/^image\//)) return callback(file);

      // PROPOSE CROPING
      uifyCrop({
        image: file,
        shape: "free",
        callback: callback,
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  RESIZE TO THE GIVEN DIMENSIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  resizeImage: {
    description: "Resize an image to the specified dimensions (will not change aspect ratio or resize smaller images, it's just the maximum width and height the image should be able to have).",
    arguments: [
      {
        label: "maxWidth",
        inputifyType: "number",
        help: "The maximum width the image can have.",
      },
      {
        label: "maxHeight",
        inputifyType: "number",
        help: "The maximum height the image can have.",
      },
      {
        label: "imageType",
        inputifyType: "radio",
        help: "Use png if your image has transparencies. Use jpg for better size optimization. Or leave blank so that the uploaded file's type is used.",
        choices: ["jpeg", "png"],
        defaultValue: "jpg",
      },
      {
        label: "quality",
        inputifyType: "slider",
        help: "[optional] Jpeg quality index for the result image (default is 0.92).",
        slider: {
          min: 0,
          max: 1,
          step: 0.01,
        },
        defaultValue: 0.92,
      },
    ],
    method: function (zeusArgs, file, callback) {

      // IS NOT IMAGE, JUST SKIP RESIZING
      if (!file.type || !file.type.match(/^image\//)) return callback(file);

      // RESIZE IMAGE
      resizeImage({
        file: file,
        maxWidth: zeusArgs[0],
        maxHeight: zeusArgs[1],
        imageType: zeusArgs[2],
        quality: zeusArgs[3],
        callback: callback,
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PROMPT IF WANT TO RESIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  resizePrompt: {
    description: "Prompt if image should be resized and propose dimensions.",
    arguments: null,
    method: function (zeusArgs, file, callback) {
      var inputified = this;

      // IS NOT IMAGE, JUST SKIP RESIZING
      if (!file.type || !file.type.match(/^image\//)) return callback(file);

      // PROPOSE RESIZING
      uify.menu({
        title: "Do you want to resize this image?",
        buttons: [
          {
            title: "no",
            resizeValue: false,
          },
          {
            title: "very small (400x400)",
            resizeValue: [400, "jpg", 0.8],
          },
          {
            title: "small (800x800)",
            resizeValue: [800, "jpg", 0.8],
          },
          {
            title: "normal (1540x1540)",
            resizeValue: [1540, "jpg", 0.8],
          },
          {
            title: "big (2000x2000)",
            resizeValue: [2000],
          },
        ],
        click: function (buttonOptions) {
          if (!buttonOptions.resizeValue) callback(file)
          else resizeImage({
            file: file,
            maxWidth: buttonOptions.resizeValue[0],
            maxHeight: buttonOptions.resizeValue[0],
            imageType: buttonOptions.resizeValue[1],
            quality: buttonOptions.resizeValue[2],
            callback: callback,
          });
        },

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("inputify.fileProcess"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  IMAGE RESIZER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: resize and optimize the given image
  ARGUMENTS: ({
    !file: <File>,
    !maxWidth: <integer>,
    !maxHeight: <integer>,
    ?imageType: <"jpeg"|"jpg"|"png"> « if not defined, will figure out type from file »,
    ?quality: <number> « between 0 and 1 (used only if imageType is jpeg) »,
    !callback: <function(<Blob>)>,
  })
  RETURN: <void>
*/
function resizeImage (options) {

  var $canvas = $("<canvas>");
  var canvas = $canvas[0];
  var ctx = canvas.getContext("2d");

  // add image
  var img = new Image();
  img.onload = function () {

    // CALCULATE THE RATIO WITH WHICH THE IMAGE SHOULD BE REDUCED
    var reductionRatio = Math.min(options.maxWidth / img.width, options.maxHeight / img.height);

    // IF RATIO IS BIGGER THAN ONE, JUST KEEP THE IMAGE SIZE AS IS
    if (reductionRatio >= 1) reductionRatio = 1;

    var newWidth = img.width * reductionRatio;
    var newHeight = img.height * reductionRatio;

    // set both canvas dimensions to the ones of the image
    canvas.width = newWidth;
    canvas.height = newHeight;

    // cleanup canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // draw resized image
    ctx.drawImage(img, 0, 0, newWidth, newHeight);

    // callback with new image
    var imageType = options.imageType || options.file.type;
    if (imageType === "png" || imageType === "image/png") canvas.toBlob(options.callback)
    else canvas.toBlob(options.callback, "image/jpeg", options.quality);

  };

  // set image source from file
  img.src = URL.createObjectURL(options.file);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
