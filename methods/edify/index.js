
module.exports = {
  beforeRemove: require("./beforeRemove"),
  beforeSave: require("./beforeSave"),
  menubarButtonClick: require("./menubarButtonClick"),
  menubarButtonCondition: require("./menubarButtonCondition"),
};
