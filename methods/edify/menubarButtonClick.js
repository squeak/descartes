var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var customMethod = require("../../customMethod");
var presarg = require("../../presetArguments");
var wikipediaFetcher = require("../../utilities/wikipediaFetcher");
var duckduckgoSearcher = require("../../utilities/duckduckgoSearcher");
var subjectAndMonitoring = require("../../utilities/subjectAndMonitoring");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FILL FROM WIKIPEDIA
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function fillFromWikipedia (postified, wikiFieldDeepKey, presetOrFields, lang) {


  //
  //                              FIGURE OUT THE URL TO THE WIKIPEDIA ARTICLE

  var wikiArticleUrlOrName = $$.getValue(postified.resultEntry, wikiFieldDeepKey);
  if (!wikiArticleUrlOrName) var wikiArticleUrlOrName = prompt("Please fill here the url or name of the wikipedia article you want to fetch values from.");
  if (!wikiArticleUrlOrName) return
  else if (!_.isString(wikiArticleUrlOrName)) return alert("The url you passed is not a string.");

  //
  //                              CREATE FETCHING SPINNER

  var fetchingSpinner = uify.spinner({
    $container: postified.$window,
    text: "Fetching wikipedia article content...",
    overlay: true,
    size: 13,
  });

  //
  //                              FETCH USING PRESET OR NORMAL MODE

  if (_.isArray(presetOrFields)) var wikifetcher = wikipediaFetcher(wikiArticleUrlOrName, presetOrFields, lang)
  else if (_.isString(presetOrFields)) var wikifetcher = wikipediaFetcher.preset(wikiArticleUrlOrName, presetOrFields)
  else {
    // destroy spinner
    fetchingSpinner.destroy();
    // display error message
    uify.alert.error("Aborted. You should pass either a preset name, or a list of fields to extract from wikipedia.<br>See logs for more details.");
    $$.log.error("Aborted. You should pass either a preset name, or a list of fields to extract from wikipedia.", presetOrFields);
    return;
  };

  //
  //                              ADD FETCHED RESULTS TO POSTIFY WINDOW

  wikifetcher.then(function (result) {
    // add wiki url to result, so it's added to entry if necessary
    if (wikiFieldDeepKey) {
      // if only name was passed, convert it to a wiki url
      if (!wikiArticleUrlOrName.match(/https?\:\/\//)) wikiArticleUrlOrName = "https://wikipedia.org/wiki/"+ wikiArticleUrlOrName.replace(/ /g, "_");
      $$.setValue(result, wikiFieldDeepKey, wikiArticleUrlOrName);
    };
    // set values in postified
    postified.inputified.set(result, { isPartialSet: true, });
    // destroy spinner
    fetchingSpinner.destroy();
  }).catch(function (err) {
    // destroy spinner
    fetchingSpinner.destroy();
    // alert any error
    uify.alert.error("Error fetching wikipedia content.<br>See logs for more details.");
    $$.log.error("Error fetching wikipedia content:", err);
  });

  //                              ¬
  //

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  ARGUMENTS: (
    e <event> « the click event fired when button was clicked »,
  ){@this=<postified>}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FILL FROM WIKIPEDIA
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  fillFromWikipedia: {
    description: "Fill entry from values in wikipedia.",
    arguments: [
      {
        label: "wikiUrlDeepKey",
        type: "normal",
        help: "Deep key of where the url to the wiki page is stored in entry.<br>If you ommit this, the url will be prompted on click.",
      },
      {
        label: "fieldsToExtract",
        type: "array",
        help: "Add here the list of fields you want to extract from the wikipedia article. To explore how the info is strucutred in the specific wikipedia articles you want to process, you can check:\nhttps://dbpedia.org/page/&lt;your_article_title&gt;",
        object: {
          model: {
            type: "object",
            object: {
              structure: [
                {
                  key: "dbpediaKey",
                  type: "normal",
                  help: "The key of the value to extract in wikipedia article.",
                },
                {
                  key: "localKey",
                  type: "normal",
                  help: "The key where to store the extracted value in your local entry.",
                },
                // {
                //   key: "process",
                //   type: "normal",
                //   <function(value):<any>> « the return of this function will define the value for this key if you don't want it raw as it's set in wikipedia »,
                // },
              ],
            },
          },
        },
      },
      {
        label: "lang",
        type: "normal",
        help: "Fill here the language code of the language you want the content to be fetched in.\nIf you leave this empty, or the content is not available in that language, will default to english.\nExample languages codes: 'en' for english, 'fr' for french...",
      },
    ],
    method: function (zeusArgs, e) {
      var postified = this;
      fillFromWikipedia(postified, zeusArgs[0], zeusArgs[1], zeusArgs[2]);
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FILL FROM WIKIPEDIA
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  fillFromWikipedia_usingPreset: {
    description: "Fill entry from values in wikipedia, using one of the predefined article type configuration.",
    arguments: [
      {
        label: "wikiUrlDeepKey",
        type: "normal",
        help: "Deep key of where the url to the wiki page is stored in entry.<br>If you ommit this, the url will be prompted on click.",
      },
      {
        label: "articleType",
        type: "select",
        choices: ["film"],
        help: "The type of article that is fetched in wikipedia (this is used to determine which fields to import from it).",
      },
    ],
    method: function (zeusArgs, e) {
      var postified = this;
      fillFromWikipedia(postified, zeusArgs[0], zeusArgs[1]);
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SEARCH IN DUCKDUCKGO
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  searchInDuckDuckGo: {
    description: "Search for this entry in duckduckgo.",
    arguments: [
      presarg.duckSearcher.deepKeys,
      presarg.duckSearcher.otherValuesToSearch,
    ],
    method: function (zeusArgs, e) {
      var postified = this;

      var valuesToSearch = _.map(zeusArgs[0], function (deepKey) {
        return $$.getValue(postified.resultEntry, deepKey);
      });

      duckduckgoSearcher([valuesToSearch, zeusArgs[1]]);

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MONITORING REFRESH SUBJECTS REPARTITION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  monitoringDispatchSubjects: {
    description: "This custom method is specifically designed for monitoring types, it will figure out where to dispatch subjects.",
    arguments: null,
    method: function (zeusArgs, e) {
      var postified = this;
      subjectAndMonitoring.dispatchSubjectsToMonitorWithSpinner(postified);
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MONITORING COPY LIST OF TODO SUBJECTS AS TEXT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  copyTodosToClipboard: {
    description: "This custom method is specifically designed for monitoring types, it will copy to clipboard the list of todos.",
    arguments: null,
    method: function (zeusArgs, e) {
      var postified = this;
      subjectAndMonitoring.copyTodosToClipboard(postified.inputified.get(), postified.storage.collection);
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("edify.menubarButtonClick"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
