var _ = require("underscore");
var $$ = require("squeak");
var customMethod = require("../../customMethod");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  ARGUMENTS: (
    entryToRemove <couchEntry> « the entry to remove »,
    shouldRemoveCallback <function(ø)> « execute this callback if the entry should be removed »,
  ){@this=postified}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHECK AFFECTED CHILDREN TO ASK TO CONFIRM WHEN REMOVING A FOLDER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  confirmRemoveFolder: {
    description: "Asks a confirmation before removing an entry, if some of it's children should be removed.",
    arguments: null,
    method: function (zeusArgs, entryToRemove, shouldRemoveCallback) {
      var postified = this;
      var Collection = postified.storage.collection;

      var listOfChildrenModelsThatAreImpacted = Collection.getFullListOfChildrenAffectedByRemovalOfFolder(entryToRemove);
      if (!listOfChildrenModelsThatAreImpacted.toRemove.length) shouldRemoveCallback()
      else {
        $$.log.group("Confirm remove folder: ", entryToRemove);
        $$.log.info("Children entries to remove:");
        $$.log.info(_.pluck(listOfChildrenModelsThatAreImpacted.toRemove, "attributes"));
        $$.log.info("Children entries to modify:");
        $$.log.info($$.pluck(listOfChildrenModelsThatAreImpacted.toModify, "model.attributes"));
        $$.log.groupEnd();
        if (confirm("Remove this folder?\nIt contains "+ listOfChildrenModelsThatAreImpacted.toRemove.length +" entry/entries that have no other path and will be removed as well.\nSee logs for details.")) shouldRemoveCallback();
      };

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("edify.beforeRemove"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
