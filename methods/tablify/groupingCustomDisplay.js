var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/color");
require("squeak/extension/date");
var subjectAndMonitoring = require("../../utilities/subjectAndMonitoring");
var customMethod = require("../../customMethod");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  ARGUMENTS: (
    $groupingIntertitleDiv <yquerjObject>,
    groupIntertitle <any> « the intertitle that would be displayed by default »,
  ){@this=<tablifyApp>}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SUBJECTS AND MONITORING CUSTOM GROUPING DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  subjectsAndMonitoring: {
    description: "This custom method is specifically designed for customizing intertitle display in subjects and monitoring databases.",
    arguments: null,
    method: function (zeusArgs, $groupingIntertitleDiv, groupIntertitle) {
      var tablifyApp = this;

      if (_.isString(groupIntertitle)) var intertitleArray = groupIntertitle.split(":");

      //
      //                              GROUPING BY CONTEXT

      // color grouping intertitle according to context
      if (intertitleArray[1]) $groupingIntertitleDiv.css("background-color", $$.color.anyString2HexColor(intertitleArray[1]));

      // INTERTITLE FOR SUBJECTS
      if (intertitleArray && intertitleArray[0] === "subject") {

        // priority text
        $groupingIntertitleDiv.span({ text: intertitleArray[1], });

        // frequency and durations text
        var entriesWithThisContext = _.filter(tablifyApp.Collection.toJSON(), function (entry) { return $$.match(entry.context, $$.regexp.make("/^"+ intertitleArray[1] +"/")); });
        var durationsForThisPriority = [];
        _.each(["multidaily", "daily", "otherdaily", "weekly", "monthly", "quarterly", "yearly", "occasional"], function (frequency) {
          var entriesWithThisContextAndFrequency = _.where(entriesWithThisContext, { frequency: frequency, });
          if (entriesWithThisContextAndFrequency.length) {
            var durationsSum = _.reduce(entriesWithThisContextAndFrequency, function (memo, entry) { return memo + subjectAndMonitoring.durationCalculator(entry.duration); }, 0);
            if (durationsSum) durationsForThisPriority.push("<i>"+ frequency +"</i>: "+ $$.date.moment.duration(durationsSum *60*1000).humanize());
          };
        });
        if (durationsForThisPriority.length) $groupingIntertitleDiv.div({
          class: "descartes-tablify-subjects_and_monitoring-durations",
          htmlSanitized: durationsForThisPriority.join(", "),
        });

      }

      // INTERTITLE FOR OTHERS
      else $groupingIntertitleDiv.html(intertitleArray && intertitleArray[1] ? intertitleArray[1] : groupIntertitle);

      //                              ¬
      //

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("tablify.groupingCustomDisplay"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
