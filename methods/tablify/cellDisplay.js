var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var uify = require("uify");
require("uify/plugin/flagify");
var pouchEntries = require("../../utilities/pouch/entries");
var presarg = require("../../presetArguments");
var multinterpolate = require("../../utilities/multinterpolate");
var customMethod = require("../../customMethod");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTIONS:
    create custom content for a cell
    you can either return the content you want to append to the cell, or append it yourself
  ARGUMENTS: (
    model <backboneModel> « the model to display »,
    deepKey <string|number> « the column key of the current cell to display »,
    $cell <yquerjObject> « the cell in which to create the content »,
  ){@this=tablifyApp}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  COLOR THE CELL WITH A GIVEN COLOR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  color: {
    description: "Color the cell with the color in the entry at the chosen key (if you don't pass a key, will use the column key).",
    arguments: [
      {
        label: "colorKey",
        type: "normal",
        help: "The deep key where to find the color in the entry. If undefined, will use the current key.",
      },
      {
        label: "fallbackColor",
        type: "color",
        help: "A fallback color if no value is found at the specified key.",
      },
    ],
    method: function (zeusArgs, model, deepKey, $cell) {
      var color = model.getValue(zeusArgs[0] || deepKey);
      if (!color && zeusArgs[1] && _.isString(zeusArgs[1])) color = zeusArgs[1];
      if (_.isString(color)) $cell.css($$.color.contrastStyle(color))
      else $$.log.error("descartes:tablify:cellDisplay You tried to set a color that is not valid: ", color)
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FLAG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  countryFlag: {
    description: "Display the flag of the country/countries set at the given key.",
    arguments: [
      {
        label: "countryKey",
        type: "normal",
        help: "The deep key where to find the country/countries names in the entry. If undefined, will use the current key.",
      },
    ],
    method: function (zeusArgs, model, deepKey, $cell) {
      var countryKey = zeusArgs[0] || deepKey;
      var countries = model.getValue(countryKey);
      if (_.isString(countries)) countries = [countries];
      _.each(countries, function (country) {
        $cell.div({
          htmlSanitized: uify.flagify(country, { size: "s", }),
        });
      });
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE A CELL CONTENT FROM STRING INTERPOLATED WITH THIS ENTRY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  interpolatedString: {
    description: "Make a cell content from a string that will be interpolated, filling with some values from this entry.",
    arguments: [
      presarg.interpolate.string,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, model, deepKey, $cell) {
      return $$.string.interpolate({
        string: zeusArgs[0],
        entry: model.attributes,
        fallback: zeusArgs[1],
        missing: zeusArgs[2],
        complexExpressions: true,
      });
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY OTHER ENTRIES VALUES FROM OTHER DATABASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valuesInOtherEntry: {
    description: "Make a cell content from a string that will be interpolated, filling with some values from this or some other entries in this database.",
    arguments: [
      presarg.deepKey.whereIdStored,
      presarg.interpolate.stringMulti,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, model, deepKey, $cell) {
      var tablifyApp = this;

      // get list of other entries ids
      var idOrIds = _.chain(zeusArgs[0]).map(function (deepKey) { return model.getValue(deepKey); }).flatten().uniq().value();
      var otherEntries = tablifyApp.Collection.gets(idOrIds, true);

      // make interpolated string
      var resultString = multinterpolate({
        string: zeusArgs[1],
        fallback: zeusArgs[2],
        missing: zeusArgs[3],
        entry: {
          entry: model.attributes,
          others: otherEntries,
        },
      });

      // parse string in cell
      $cell.htmlSanitized(resultString);

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY OTHER ENTRIES VALUES FROM OTHER DATABASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valuesInOtherEntryInOtherDatabase: {
    description: "Make a cell content from a string that will be interpolated, filling with some values from this or some other entries in another database.",
    arguments: [
      presarg.database.choose,
      presarg.deepKey.whereIdStored,
      presarg.interpolate.stringMulti,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, model, deepKey, $cell) {
      var tablifyApp = this;

      // get list of other entries ids
      var idOrIds = _.chain(zeusArgs[0]).map(function (deepKey) { return model.getValue(deepKey); }).flatten().uniq().value();

      // get other entries
      pouchEntries.gets(zeusArgs[0], idOrIds, function (otherEntries) {

        // make interpolated string
        var resultString = multinterpolate({
          string: zeusArgs[2],
          fallback: zeusArgs[3],
          missing: zeusArgs[4],
          entry: {
            entry: model.attributes,
            others: otherEntries,
          },
        });

        // parse string in cell
        $cell.htmlSanitized(resultString);

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("tablify.cellDisplay"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
