var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/color");
require("squeak/extension/date");
require("squeak/extension/string");
var uify = require("uify");
require("uify/extension/swiper");
require("uify/extension/preview");
require("uify/plugin/flagify");
var presarg = require("../../presetArguments");
var pouchAttachments = require("../../utilities/pouch/attachments");
var pouchEntries = require("../../utilities/pouch/entries");
var subjectAndMonitoring = require("../../utilities/subjectAndMonitoring");
var markdownify = require("markdownify");
var customMethod = require("../../customMethod");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SUBJECTS STATS FOR A GIVEN TIME PERIOD
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var timeSpans = [
  {
    name: "continuous",
    frequencies: ["multidaily", "daily", "otherdaily"],
    duration: "4h",
    calculation: function (frequency, duration) {
      if (frequency === "otherdaily") return duration / 2
      else return duration;
    },
  },
  {
    name: "regular",
    frequencies: ["weekly"],
    duration: "4h",
    calculation: function (frequency, duration) {
      return duration / 5;
    },
  },
  {
    name: "occasional",
    frequencies: ["monthly", "quarterly", "yearly", "occasional"],
    duration: "4h",
    calculation: function (frequency, duration) {
      return duration / 30;
    },
  },
];

var lists = {
  todo: "#0F0",
  totry: "#1effb9",
  toflex: "#f2d4ff",
};

var graphsList = {
  durationPlan: "Planned duration",
  durationDone: "Duration",
};

/**
  DESCRIPTION: display stats of subjects in the given frequency
  ARGUMENTS: (
    tablifyApp,
    $container,
    entry <{
      todo: <subjectsList>,
      totry: <subjectsList>,
      toflex: <subjectsList>,
    }>,
    timeSpanObject <{
      name: <string>,
      frequencies: <<"multidaily"|"daily"|"otherdaily"|"weekly"|"monthly"|"quarterly"|"yearly"|"occasional">[]>,
      duration: <string> « e.g. "4h" »,
    }>,
  )
  RETURN: <void>
  TYPES:
    subjectsList = <{
      subject: <string> « string shape: "contextGroup:context/subjectName" »,
      todo: <"toprocess"|"todo"|"todont">,
      priority: <"log"|"normal"|"high">,
      frequency: <"multidaily"|"daily"|"otherdaily"|"weekly"|"monthly"|"quarterly"|"yearly"|"occasional">,
      duration: <string>,
    }[]>
*/
function frequencyStats (tablifyApp, $container, entry, timeSpanObject) {

  // o            >            +            #            °            ·            ^            :            |
  //                                           KEEP ONLY RELEVANT SUBJECTS

  // get only subjects with this frequency
  var subjectsInTimespans = _.mapObject(lists, function (listColor, listName) {
    return _.filter(entry[listName], function (subjObj) {
      if (!subjObj) return;
      return _.indexOf(timeSpanObject.frequencies, subjObj.frequency) !== -1;
    });
  });

  // o            >            +            #            °            ·            ^            :            |
  //                                           GENERATE STATS

  var allDurationsSums = _.mapObject(subjectsInTimespans, function (subjectsInThisTimespan, listName) {
    return _.mapObject(graphsList, function (durTitle, durKey) {
      return _.reduce(subjectsInThisTimespan, function (memo, entry) {
        var durationInMinutes = subjectAndMonitoring.durationCalculator(entry[durKey]);
        return memo + timeSpanObject.calculation(entry.frequency, durationInMinutes);
      }, 0);
    });
  })

  // o            >            +            #            °            ·            ^            :            |
  //                                           DISPLAY

  // create container
  var $timePeriod = $container.div({ class: "descartes-viewify-subjects_and_monitoring-field", });

  // timespan title
  $timePeriod.div({
    class: "title",
    htmlSanitized: timeSpanObject.name +' <span class="title-details">('+ timeSpanObject.frequencies.join(", ") +')</span>',
  });

  //
  //                              DISPLAY LIST OF SUBJECTS

  var $subjects = $timePeriod.div({ class: "subjects", });
  _.each(subjectsInTimespans, function (subjectsInThisTimespan, listName) {
    if (subjectsInThisTimespan && subjectsInThisTimespan.length) {

      // subjects title
      $subjects.div({
        class: "subjects-title",
        text: listName +":",
      }).css({
        backgroundColor: lists[listName],
      });

      // subjects list
      var $subjectsList = $subjects.div({ class: "subjects-list", });
      _.each(subjectsInThisTimespan, function (subjObj) {
        if (!subjObj) return;
        var subjectModel = tablifyApp.Collection.get(subjObj.subject)
        var subjectContext = subjectModel.get("context");
        var subjectContextGroup = subjectContext.replace(/\:[^\:]*$/, "");
        var subjectContextColor = $$.color.anyString2HexColor(subjectContextGroup);
        $subjectsList
        .span({ text: subjectContext +"/"+ subjectModel.get("name"), })
        // color accordingly to context
        .css({
          color: subjectContextColor,
          // backgroundColor: $$.color.contrast(subjectContextColor),
        })
        // click to open subject entry
        .click(function () {
          tablifyApp.openEntry(subjectModel);
        })
        ;
      });
    };
  });

  //
  //                              DISPLAY STATS

  var durationMax = subjectAndMonitoring.durationCalculator(timeSpanObject.duration);
  _.each(graphsList, function (durTitle, durKey) {
    var $graph = $timePeriod.div({ class: "graph", });
    var $graphBars = $graph.div({ class: "graph-bars", });
    var sumOfDurations = 0;

    // display durations bars
    _.each(allDurationsSums, function (durationsSums, listName) {
      var durationsSum = durationsSums[durKey];
      sumOfDurations += durationsSum;

      // make graph bar
      $graphBars.div({
        class: "graph-bar",
        title: listName +" ("+ $$.date.moment.duration(durationsSum *60*1000).humanize() +")",
      }).css({
        width: ((durationsSum / durationMax ) *100) +"%",
        backgroundColor: lists[listName],
      });

    });

    // display subjects duration stats as text
    $graph.div({
      class: "stats_text",
      text: durTitle +": "+
        (sumOfDurations > 0 ? $$.date.moment.duration(sumOfDurations *60*1000).humanize() : 0)
        +" / "+
        $$.date.moment.duration(durationMax *60*1000).humanize()
      ,
    });

  });

  //                              ¬
  //

  // o            >            +            #            °            ·            ^            :            |

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    a custom way to display this key's value
    if this function is defined, will not get value by key's name
    if this function returns something, it will be appended to the field
    you can also just append things yourself to the dom elements with this function (but for that you could also consider using fieldProcess DOM maniuplations functions)
    if your method is asynchronous, make sure to return "async", otherwise nothing to return
  ARGUMENTS: (
    entry <couchEntry> « current entry »,
    $fieldValue <yquerjObject> « the container where to put values in this $field »,
    $field <yquerjObject> « the global container of this field »,
    deepKey <string|number> « the deepKey of this value »,
    asyncCallback <function(ø)> « if your method is asynchronous, make sure to run this callback when it finishes »,
  ){@this=viewified}
  RETURN: <undefined|"async">
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TEMPLATE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  template: {
    description: "Make display from a complex template.",
    arguments: [
      presarg.interpolate.stringLong,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {

      // create asked content from entry
      $fieldValue.htmlSanitized(
        $$.string.interpolate({
          string: zeusArgs[0],
          entry: entry,
          fallback: zeusArgs[1],
          missing: zeusArgs[2] === null ? "" : zeusArgs[2],
          complexExpressions: true,
        })
      );

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TEMPLATE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  templateIterator: {
    description: [
      "Make display from a complex template that will be applied to all array/object entries of the given key.",
      "In this situation, the root of the template variables is not the entry, but the iterated subvalue in the object/array.",
    ],
    arguments: [
      presarg.interpolate.stringLong,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {

      _.each($$.getValue(entry, deepKey), function (subEntry) {

        // create asked content from subentry
        $fieldValue.div({
          class: "descartes-viewify-field_display-template_iterator-subentry",
          htmlSanitized: $$.string.interpolate({
            string: zeusArgs[0],
            entry: subEntry,
            fallback: zeusArgs[1],
            missing: zeusArgs[2] === null ? "" : zeusArgs[2],
            complexExpressions: true,
          })
        });

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE A CLICKABLE LINK
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  link: {
    description: "Make a clickable link.",
    arguments: [
      {
        label: "prefix",
        labelLayout: "hidden",
        type: "normal",
        help: "Some text before the clickable link.",
        input: { placeholder: "prefix to clickable text", },
      },
      {
        label: "text",
        labelLayout: "hidden",
        type: "normal",
        input: { placeholder: "text that clicked will open link", },
        help: "The text that will be clickable. If this is not defined, will use the key of this view field.",
      },
      {
        label: "suffix",
        labelLayout: "hidden",
        type: "normal",
        input: { placeholder: "suffix to clickable text", },
        help: "Some text after the clickable link.",
      },
    ],
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      var url = $$.getValue(entry, deepKey);
      if (url) $fieldValue.htmlSanitized(
        (zeusArgs[0] || "") + '<a href="'+ url +'" target="_blank">'+ (zeusArgs[1] || deepKey) +'</a>' + (zeusArgs[2] || "")
      );
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE MULTIPLE CLICKABLE LINKS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  linksObject: {
    description: "Use this to display multiple links from an object containing linkText/linkUrl pairs.",
    arguments: null,
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      var hrefs = $$.getValue(entry, deepKey);
      $fieldValue.htmlSanitized(
        _.map(hrefs, function (href, text) {
          return '<a href="'+ href +'" target="_blank" class="descartes-viewify-link">'+ text +'</a>';
        }).join(" ")
      );
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MULTILINE LIST
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  multilineList: {
    description: "Use this to display an array's values, each value on it's own line.",
    arguments: null,
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      var lines = $$.getValue(entry, deepKey);
      if (_.isArray(lines)) $fieldValue.htmlSanitized(lines.join("<br>"));
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  HTML STRING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  htmlString: {
    description: "Use this to display an html string stored at this key.",
    arguments: null,
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      $fieldValue.htmlSanitized(
        $$.getValue(entry, deepKey)
      );
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE A TABBED VIEW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // tabbed: {
  //   description: "Display multiple values in tabs",
  //   // arguments: // any ammount: list of keys
  //   argumentsModel: {
  //     // TODO
  //   },
  //   method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
  //
  //     uify.tabs({
  //       $container: $field,
  //       tabs: _.map(zeusArgs, function (key) {
  //         return {
  //           name: key,
  //           content: $$.getValue(entry, key),
  //         };
  //       }),
  //     });
  //
  //   },
  // },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VALUE IN THIS DATABASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valueFromEntryInThisDatabase: {
    description: "Display some value(s) from another entry in this database.",
    arguments: [
      presarg.interpolate.string,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      var viewified = this;

      // get asked db entry
      var otherEntryId = $$.getValue(entry, deepKey);
      if (!otherEntryId) return;
      var otherEntry = viewified.storage.tablifyApp.Collection.get(otherEntryId);
      if (!otherEntry) return;

      // show asked content from entry
      $fieldValue.htmlSanitized(
        $$.string.interpolate({
          string: zeusArgs[0],
          entry: otherEntry.attributes,
          fallback: zeusArgs[1],
          missing: zeusArgs[2] === null ? "" : zeusArgs[2],
          complexExpressions: true,
        })
      );

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VALUE IN OTHER DATABASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  valueFromEntryInOtherDatabase: {
    description: "Display some value(s) from another entry in another database.",
    arguments: [
      presarg.database.choose,
      presarg.interpolate.string,
      presarg.interpolate.fallback,
      presarg.interpolate.missing,
    ],
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey, asyncCallback) {
      var viewified = this;

      // get other entry id
      var otherEntryId = $$.getValue(entry, deepKey);
      if (!otherEntryId) return;

      // fetch other entry
      pouchEntries.get(zeusArgs[0], otherEntryId, function (otherEntry) {

        // show asked content from entry
        if (otherEntry) $fieldValue.htmlSanitized(
          $$.string.interpolate({
            string: zeusArgs[1],
            entry: otherEntry,
            fallback: zeusArgs[2],
            missing: zeusArgs[3] === null ? "" : zeusArgs[2],
            complexExpressions: true,
          })
        );

        // notify viewify that async completed
        asyncCallback();

      });

      // specify to viewify that this display function is asynchronous
      return "async";

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY AN ATTACHED IMAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  attachedImage: {
    description: "Display one or many image attached to this entry.",
    arguments: [
      {
        label: "image_css",
        inputifyType: "object",
        help: "Custom css rules to apply to the image.",
      },
      {
        label: "title_css",
        inputifyType: "object",
        help: "Custom css rules to apply to the image title.",
      },
    ],
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {

      // get list of images
      var images = pouchAttachments.getAttachedImages(entry, deepKey);

      // display them
      _.each(images, function (image) {

        var $image = $fieldValue.img({
          src: image.url,
          class: "descartes-viewify-image",
        });
        if (zeusArgs[0]) $image.css(zeusArgs[0]);

        // image title
        if (image.title) {
          var $title = $fieldValue.div({ text: image.title, }).css("text-align", "right");
          if (zeusArgs[1]) $title.css(zeusArgs[1]);
        };

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ATTACHED IMAGES, SWIPING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  attachedImagesSwipable: {
    description: "Display some images attached to this entry in a swipable area.",
    arguments: [
      {
        label: "image_css",
        inputifyType: "object",
        help: "Custom css rules to apply to the images swiper container.",
      },
    ],
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {

      var attachedImages = pouchAttachments.getAttachedImages(entry, deepKey)

      if (attachedImages && attachedImages.length) {

        // create images swiper
        var swiper = uify.swiper({
          $target: $fieldValue,
          slides: _.pluck(attachedImages, "url"),
          slidesAreImageUrls: true,
        });

        // add class to customize display of swiper
        $fieldValue.addClass("descartes-viewify-image_swiper");

        // custom css rules to apply to container
        if (zeusArgs[0]) $fieldValue.css(zeusArgs[0]);

        // this is probably very bad, but it seems that swiper is created before being visible and it causes it not to swipe properly
        setTimeout(function () {
          swiper.swiper.update();
        }, 100);

      };

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ANY ATTACHMENTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  anyAttachments: {
    description: "Display one or multiple attachments that can be previewed or downloaded.",
    arguments: [
      {
        label: "iconOnly",
        inputifyType: "boolean",
        help: "Set this to true if you don't want to display small swipable previews, but only an icon that opens preview on click.",
      },
    ],
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {

      $field.addClass("descartes-viewify-field_display-any_attachments");
      if (zeusArgs[0]) $field.addClass("descartes-viewify-field_display-any_attachments_condensed");

      var attachedFiles = pouchAttachments.getAttachedImages(entry, deepKey);

      if (attachedFiles.length) uify.preview({
        content: _.map(attachedFiles, function (attachmentObject) {
          return {
            content: attachmentObject.url,
            contentType: $$.match(attachmentObject.url, /^data\:[^;]*/).replace(/^data\:/, ""),
            title: attachmentObject.title,
          };
        }),
        $clickableThumbnailContainer: $fieldValue,
        displayOnlyIconThumbnail: zeusArgs[0],
        callback: function (uifyPreview) {
          // correct swiper dimensions // FIXME: maybe this could be avoided using "init" or "height" option of swiperjs
          if (!zeusArgs[0]) {
            var appropriateWidth = $field.parents(".windify").width() - 20 - 10; // remove padding of field parents elements (2*10 for body and 2*5 for field)
            $fieldValue.width(appropriateWidth);
            $fieldValue.height(appropriateWidth * 9/16);
            // this is probably very bad, but it seems that swiper is created before being visible and it causes it not to swipe properly
            setTimeout(function () {
              if (uifyPreview.reduced) uifyPreview.reduced.swiper.update();
            }, 100);
          };
        },
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY SOME MARKDOWN CONTENT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  markdown: {
    description: "Display the markdown-formatted string in a nice way.",
    arguments: null,
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      $fieldValue.htmlSanitized(
        markdownify($$.getValue(entry, deepKey))
      );
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FLAG/FLAGS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  flags: {
    description: "Display one or multiple country flags.",
    arguments: [
      {
        label: "size",
        help: 'Size of the flag(s).',
        type: "radio",
        choices: ["xs", "s", "m", "l", "xl"],
      },
    ],
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      var countries = _.flatten([$$.getValue(entry, deepKey)]);
      _.each(countries, function (country) {
        $fieldValue.div({
          htmlSanitized: uify.flagify(country, { size: zeusArgs[0] || "m", }),
        });
      });
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ICON/ICONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  icons: {
    description: "Display one or multiple icons.",
    arguments: null,
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      var icons = _.flatten([$$.getValue(entry, deepKey)]);
      _.each(icons, function (icon) {
        $fieldValue.div({ class: "icon-"+ icon, });
      });
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  COPYABLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  tapToCopy: {
    description: "Display the content of a value, making it copyable to clipboard when clicked.",
    arguments: null,
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      var value = $$.getValue(entry, deepKey);
      $fieldValue.htmlSanitized(
        $$.string.make(value, { htmlifyObjects: true, })
      ).click(function () {
        var copiedText = _.isObject(value) ? $$.json.pretty(value) : value;
        $$.copyToClipboard(copiedText);
        uify.toast('Copied "'+ copiedText +'" to clipboard.');
      }).css("cursor", "pointer");
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TAGS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  tagsFilteringDatabaseOnClick: {
    description: "Display a list of values like clickable tags. When you click on them, the database is sorted to find issues having the given value at this key.",
    arguments: null,
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      var viewified = this;

      var values = $$.getValue(entry, deepKey);
      if (_.isUndefined(values)) return
      else if (!_.isArray(values)) values = [values];

      // display a little tag button for each value
      _.each(values, function (value) {

        $fieldValue.div({
          class: "descartes-viewify-tags_filtering_on_click",
          htmlSanitized: value,
        }).click(function () {

          // filter database entries that have this value
          viewified.storage.tablifyApp.filterAll(function (dbEntryModel) {
            return _.indexOf(dbEntryModel.getValue(deepKey), value) !== -1;
          });

        });

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY MONITORING STATS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  subjectsMonitoringStats: {
    description: "This custom method is specifically designed for showing stats of subjects monitoring.",
    arguments: null,
    method: function (zeusArgs, entry, $fieldValue, $field, deepKey) {
      var viewified = this;
      _.each(timeSpans, function (timeSpanObject) {
        frequencyStats(viewified.storage.tablifyApp, $fieldValue, entry, timeSpanObject);
      });
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("viewify.fieldDisplay"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
