var _ = require("underscore");
var $$ = require("squeak");
var presarg = require("../../presetArguments");
var customMethod = require("../../customMethod");
var pouchEntries = require("../../utilities/pouch/entries");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LIST AVAILABLE DATA ENTRIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: list available data entries (created by the user in it's types database)
  ARGUMENTS: (
    !callback <function(<inputify·options.choices>),
  )
  RETURN: <void>
*/
function availableDataEntries (callback) {

  pouchEntries.fetchAll("dato", {
    type: "_data_",
  }, function (entriesList) {
    callback(_.map(entriesList, function (entry) {
      return {
        _isChoice: true,
        value: entry._id,
        label: entry.name,
        search: entry.name,
      };
    }));
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  ARGUMENTS: (
    callback <function> «
      a callback to run when your function has finished what it had to do
      ⚠ you must run this function, if you don't db won't start properly
    »,
  ){@this=<tablifyApp>}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LOAD DATA ENTRIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  loadDataEntries: {
    description: "Load some data entries and save them in session storage.",
    arguments: [
      {
        type: "suggest",
        label: "Data entries to preload:",
        help: "Data entries will be accessible running $$.storage.session(\"dato-&lt;dbName&gt;-dataEntries\")",
        choices: function (callback) { availableDataEntries(callback); },
      },
    ],
    method: function (zeusArgs, callback) {
      var App = this;

      // get asked data entries
      pouchEntries.gets("dato", zeusArgs[0], function (entries) {

        // map them to an object in the format { [name]: <{data}> }
        var dataEntriesObject = {};
        _.each(entries, function (entry) {
          dataEntriesObject[entry.name] = entry.data;
        });

        // save all objects to session storage
        $$.storage.session("dato-"+ App.config.keyboardifyContext +"-dataEntries", dataEntriesObject);

        // callback to notify tablify that method finished
        callback();

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LOAD ANOTHER DATABASE ENTRIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  loadEntries: {
    description: "Load some entries from another database and save them in session storage.",
    arguments: [
      presarg.database.choose,
      presarg.database.filter,
      {
        type: "normal",
        label: "Key where to store entries in session storage:",
        help: "Data entries will be accessible running $$.storage.session(\"dato-&lt;currentDatabaseName&gt;-&lt;chosenKey&gt;\")",
      },
    ],
    method: function (zeusArgs, callback) {
      var App = this;

      // get asked db entries
      pouchEntries.fetchAll(zeusArgs[0], zeusArgs[1], function (entries) {

        // save all entries to session storage
        $$.storage.session("dato-"+ App.config.keyboardifyContext +"-"+ zeusArgs[2], entries);

        // callback to notify tablify that method finished
        callback();

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("db.onStart"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
