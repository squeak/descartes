var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/date");
require("squeak/extension/string");
var uify = require("uify");
var customMethod = require("../../customMethod");
var descartesUtils = require("../../utilities");
var presarg = require("../../presetArguments");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  ARGUMENTS: (
    dbConfig <couchEntry> « the configuration of the database from which was called this recurring method »,
    methodIndex <integer> « the index of the method in the database recurringEvents key array »,
  ){@this=<spacified>}
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  NOTIFY ENTRIES OF THE DAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  notifyTodayEntries: {
    description: "Notify entries in the database that have current day's date.",
    arguments: [
      presarg.database.filter,
      {
        label: "key",
        help: "Key where the date is stored in entries.",
        type: "normal",
      },
      {
        label: "Message to display: ",
        labelLayout: "stacked",
        help: "The message you want the notification to display, you may use values in the entry contents with interpolation syntax, specifying {{ key }}, to insert some of the found entry's value to the message.",
        type: "textarea",
      },
      {
        label: "Notification color: ",
        labelLayout: "stacked",
        help: "The color to give to the notification.",
        type: "color",
        spectrum: { showRandomButton: "calculateLater", },
      },
      {
        label: "Notification icon: ",
        labelLayout: "stacked",
        help: "An icon to give to the notification.",
        type: "icon",
      },
    ],
    method: function (zeusArgs, dbConfig, methodIndex) {
      var spacified = this;

      // ABORT IF ALREADY NOTIFIED TODAY
      if (descartesUtils.notifications.hasAlreadyNotifiedToday(dbConfig, methodIndex)) return;

      // FILTER ENTRIES THAT HAVE TODAY'S DATE
      var fullFilter = zeusArgs[0] || {};
      fullFilter[zeusArgs[1]] = $$.date.moment().subtract("5", "hours").format("YYYY-MM-DD");
      descartesUtils.pouch.entries.find(dbConfig.name, fullFilter, function (entries) {

        // IF THERE ARE SOME ENTRIES FOR TODAY, SEND NOTIFICATION
        if (entries && entries.length) _.each(entries, function (entry) {
          uify.toast.desktop({
            title: "[dato ⍿ "+ dbConfig.name +"]",
            message: $$.string.interpolate({
              complexExpressions: true,
              string: zeusArgs[2],
              entry: entry,
            }) || "today entry",
            color: zeusArgs[3] === "random" ? $$.random.color(1) : zeusArgs[3],
            button: {
              inlineTitle: true,
              title: "click to open entry",
              click: function () {
                spacified.datopenDatabase(dbConfig.name, [entry._id]);
              },
            },
          });
        });

      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  NOTIFY REPEATEDLY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  regularilyNotify: {
    description: "Regularily send a notification with a custom content.",
    arguments: [
      {
        label: "Message to display: ",
        labelLayout: "stacked",
        help: "The message you want the notification to display.",
        type: "textarea",
      },
      {
        label: "Notify on days: ",
        labelLayout: "stacked",
        help: "Which day of the week notification should be sent.",
        type: "tickbox",
        choices: [ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" ],
      },
      {
        label: "Notification color: ",
        labelLayout: "stacked",
        help: "The color to give to the notification.",
        type: "color",
        spectrum: { showRandomButton: "calculateLater", },
      },
      {
        label: "Notification icon: ",
        labelLayout: "stacked",
        help: "An icon to give to the notification.",
        type: "icon",
      },
    ],
    method: function (zeusArgs, dbConfig, methodIndex) {
      var spacified = this;

      // ABORT IF NOTIFICATION SHOULD NOT BE SENT
      // not right day of week
      if (_.indexOf(zeusArgs[1], $$.date.moment().format("ddd")) === -1) return;
      // has already notified today
      if (descartesUtils.notifications.hasAlreadyNotifiedToday(dbConfig, methodIndex)) return;

      // SEND NOTIFICATION
      uify.toast.desktop({
        title: "[dato ⍿ "+ dbConfig.name +"]",
        message: $$.string.htmlify(zeusArgs[0]),
        color: zeusArgs[2] === "random" ? $$.random.color(1) : zeusArgs[2],
        icon: zeusArgs[3],
        button: {
          inlineTitle: true,
          title: "click to open database",
          click: function () {
            spacified.datopenDatabase(dbConfig.name);
          },
        },
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  NOTIFY REPEATEDLY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  regularilyProposeRandomEntry: {
    description: "Regularily send a notification to open a random entry from this database.",
    arguments: [
      presarg.database.filter,
      {
        label: "Notify on days: ",
        labelLayout: "stacked",
        help: "Which day of the week notification should be sent.",
        type: "tickbox",
        choices: [ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" ],
      },
      {
        label: "Notification color: ",
        labelLayout: "stacked",
        help: "The color to give to the notification.",
        type: "color",
        spectrum: {
          showRandomButton: "calculateLater",
        },
      },
    ],
    method: function (zeusArgs, dbConfig, methodIndex) {
      var spacified = this;

      // ABORT IF NOTIFICATION SHOULD NOT BE SENT
      // not right day of week
      if (_.indexOf(zeusArgs[1], $$.date.moment().format("ddd")) === -1) return;
      // has already notified today
      if (descartesUtils.notifications.hasAlreadyNotifiedToday(dbConfig, methodIndex)) return;

      // SEND NOTIFICATION
      uify.toast.desktop({
        title: "[dato ⍿ "+ dbConfig.name +"]",
        message: "(: surprise for you :)",
        color: zeusArgs[2] === "random" ? $$.random.color(1) : zeusArgs[2],
        icon: "shuffle",
        button: {
          inlineTitle: true,
          title: "click to open random entry",
          click: function () {

            var spinner = uify.spinner({
              text: "Loading random entry",
              overlay: true,
            });

            // use timeout for spinner to have time to show
            setTimeout(function () {

              // SELECT RANDOM ENTRY TO DISPLAY
              descartesUtils.pouch.entries.fetchAll(dbConfig.name, zeusArgs[0], function (entries) {

                var entryToOpen = $$.random.entry(entries);
                spacified.datopenDatabase(dbConfig.name, [entryToOpen._id]);
                spinner.destroy();

              });

            }, 250);

          },
        },
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("db.recurringEvents"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
