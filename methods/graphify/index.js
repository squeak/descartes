
module.exports = {
  colorMaker: require("./colorMaker"),
  dataExtraction: require("./dataExtraction"),
  filterCounts: require("./filterCounts"),
  filterEntries: require("./filterEntries"),
  groupCounts: require("./groupCounts"),
  labelDisplay: require("./labelDisplay"),
  sortCounts: require("./sortCounts"),
};
