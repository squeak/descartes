var _ = require("underscore");
var $$ = require("squeak");
var customMethod = require("../../customMethod");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE GETTER FUNCTION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeGetterFunction (getter) {

  if (_.isFunction(getter)) return getter
  else if (_.isObject(getter)) return function (entry) {
    return _.mapObject(getter, function (deepKey, label) { return $$.getValue(entry, deepKey); });
  }
  else return function (entry) { return $$.getValue(entry, getter); }

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE DATA BY COUNTING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: process collection to make stats of it
  ARGUMENTS: (
    !collectionToGraph <couchEntry[]> « collection to count »,
    !countBy <graphify·graphOptions.countBy>,
    ?additionalDivision <string> « key of additional data to get from entry to divide counts in subsets »,
    ?dropUndefinedValuesOrDivision <boolean> « if true, will not increase count if a value of division is undefined »,
  )
  RETURN: <graphify·countAndLabelObject>
*/
function countValues (collectionToGraph, countBy, additionalDivision, dropUndefinedValuesOrDivision) {

  //
  //                              MAKE FUNCTION TO USE TO COUNT ENTRIES

  // handle countby to be a function, an object or a string
  var countByFunction = makeGetterFunction(countBy);

  //
  //                              MAKE COUNTS (if value is an array, each of it's entries will be one count)

  var counts = {};
  _.each(collectionToGraph, function (entry) {

    var datasetsNames = additionalDivision ? $$.getValue(entry, additionalDivision) : "default";
    if (!_.isArray(datasetsNames)) datasetsNames = [datasetsNames];
    var values = countByFunction(entry);
    if (!_.isArray(values)) values = [values];

    _.each(datasetsNames, function (datasetName) {
      if (dropUndefinedValuesOrDivision && _.isUndefined(datasetName)) return;
      _.each(values, function (val) {
        if (dropUndefinedValuesOrDivision && _.isUndefined(val)) return;
        if (!counts[val]) counts[val] = {}
        if (!counts[val][datasetName]) counts[val][datasetName] = 1
        else counts[val][datasetName]++;
      });
    });

  });

  //
  //                              RESULT

  return counts;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE DATA BY VALUE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: process collection to make stats of it
  ARGUMENTS: (
    !collection <object[]> « collection to count »,
    !getLabels <graphify·graphOptions.getLabels>,
    !getValues <graphify·graphOptions.getValues>,
  )
  RETURN: <graphify·countAndLabelObject>
*/
function extractDataAndLabels (collectionToGraph, getLabels, getValues) {

  var counts = {};

  var getLabelsFunc = _.isFunction(getLabels) ? getLabels : function (entry) { return $$.getValue(entry, getLabels); };
  var getValuesFunc = makeGetterFunction(getValues);

  return _.map(collectionToGraph, function (entry) {
    return {
      label: getLabelsFunc(entry),
      count: getValuesFunc(entry),
    };
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION:
    extract data to graph from the collection
  ARGUMENTS: (
    collection <couchEntry[]> « collection to extract data to graph »,
  )
  RETURN: <graphify·countsAndLabelObject[]> « list of datapoints to graph »
  TYPES:
    graphify·countValue = <
      |-- <number> « if there is only one dataset in the graph, the value to use in graph for this entry »
      |-- <{ [datasetName]: <number> }> « if there are many datasets in the graph, the list of values to use in graph (organnized by dataset they belong to) »
    >
    graphify·countAndLabelObject = <{
      label: <string>,
      count: <graphify·countValue>,
    }>
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  COUNT VALUES AT GIVEN KEY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  countValuesOccurences: {
    description: "Count occurences of values present at the given key.",
    arguments: [
      {
        label: "deepKey",
        inputifyType: "normal",
        help: "Key where to find values to count.",
      },
      {
        label: "match",
        inputifyType: "normal",
        help: "If you don't want this whole value to be used for counting. You can pass here some regexp matcher to only keep a portion of the text.",
      },
      {
        label: "additionalDivision",
        inputifyType: "normal",
        help: "Deep key where to find additional values to divide counts produced.",
      },
      {
        label: "dropUndefinedValuesOrDivision",
        inputifyType: "boolean",
        help: "If true, will not increase count if a value of division is undefined.",
      },
    ],
    method: function (zeusArgs, collection) {

      return countValues(collection, function (entry) {
        var value = $$.getValue(entry, zeusArgs[0]);
        if (zeusArgs[1]) return $$.match(value +"", $$.regexp.make(zeusArgs[1]))
        else return value;
      }, zeusArgs[2], zeusArgs[3]);

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EXTRACT DATA AND LABELS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  extractDataAndLabels: {
    description: "Extract data and labels from entries.",
    arguments: [
      {
        label: "labelsDeepKey",
        inputifyType: "normal",
        help: "Deep key where to find in entries, labels to use in main graph axis.",
      },
      {
        label: "dataDeepKeys",
        inputifyType: "array",
        help: "Deep keys where to get data values to graph in entries.",
      },
    ],
    method: function (zeusArgs, collection) {

      return extractDataAndLabels(collection, zeusArgs[0], function (zeusArgs, entry) {
        var values = {};
        _.each(zeusArgs[1], function (deepKey) {
          values[deepKey] = $$.getValue(entry, deepKey);
        });
        return values;
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("graphify.dataExtraction"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
