var _ = require("underscore");
var $$ = require("squeak");
var graphutils = require("../../utilities/graphutils");
var customMethod = require("../../customMethod");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    group counts
  ARGUMENTS: (
    countObject <graphify·countAndLabelObject> « the label and count value(s) of this extracted data count element to graph »,
  )
  RETURN: <string> « the group this count should be added to (this value will be used as label of this group in main axis) »
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REGROUP COUNTS TO HAVE RANGES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  groupByRange: {
    description: "Group counts that are in same ranges of values.",
    arguments: [
      {
        label: "rangesBounds",
        inputifyType: "array",
        object: {
          model: {
            inputifyType: "number",
          },
        },
        help: "List of bounds for the different ranges of counts.",
      },
      {
        label: "datasetName",
        inputifyType: "normal",
        help: "If there are multiple datasets produced by your data extraction, which dataset should be used to group counts.",
      },
    ],
    method: function (zeusArgs, countAndLabelObject) {

      // get count number to group by
      var countNumber = graphutils.getCountNumber(countAndLabelObject.count, zeusArgs[1]);

      // make sure range bounds is defined and in order
      var rangesBounds = zeusArgs[0] || [0, 25, 50, 100, 200, 500]
      rangeBounds = _.chain(rangesBounds).sortBy(function (val) { return +val; }).reject(_.isNaN).value();

      // figure out insertion index
      var insertionIndex = _.sortedIndex(rangesBounds, countNumber);

      // make label in the form "0+ to 25", "25+ to 50" or "500+ to +∞"
      var from = !_.isUndefined(rangesBounds[insertionIndex-1]) ? rangesBounds[insertionIndex-1] +"+" : "-∞";
      var to = rangesBounds[insertionIndex] ? rangesBounds[insertionIndex] : "+∞";

      // return grouping label
      return from +" to "+ to;

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  customMethod: customMethod("graphify.groupCounts"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
